<div class="footer-menu">
    <ul>
        <?php
        $homeUrl = Yii::$app->homeUrl;

        foreach ($model as $value) {
            ?>
            <li>
                <a href="<?= $homeUrl ?><?= $value['slug'] ?>">
                    <?= $value['title'] ?>
                </a>
            </li>
            <?php
        }
        ?>
    </ul>
</div>