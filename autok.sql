/*
SQLyog Ultimate v12.14 (64 bit)
MySQL - 5.6.34-log : Database - autok
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `car_manufacturer` */

CREATE TABLE `car_manufacturer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` varchar(300) DEFAULT NULL,
  `updated_at` varchar(300) DEFAULT NULL,
  `is_status` tinyint(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=286 DEFAULT CHARSET=utf8;

/*Data for the table `car_manufacturer` */

LOCK TABLES `car_manufacturer` WRITE;

insert  into `car_manufacturer`(`id`,`name`,`created_at`,`updated_at`,`is_status`) values 
(1,'AC',NULL,NULL,NULL),
(2,'Acura',NULL,NULL,NULL),
(3,'Alfa Romeo',NULL,NULL,NULL),
(4,'Alpine',NULL,NULL,NULL),
(5,'AM General',NULL,NULL,NULL),
(6,'Ariel',NULL,NULL,NULL),
(7,'Aro',NULL,NULL,NULL),
(8,'Asia',NULL,NULL,NULL),
(9,'Aston Martin',NULL,NULL,NULL),
(10,'Audi',NULL,NULL,NULL),
(11,'Austin',NULL,NULL,NULL),
(12,'Autobianchi',NULL,NULL,NULL),
(13,'Baltijas Dzips',NULL,NULL,NULL),
(14,'Beijing',NULL,NULL,NULL),
(15,'Bentley',NULL,NULL,NULL),
(16,'Bertone',NULL,NULL,NULL),
(17,'Bitter',NULL,NULL,NULL),
(18,'BMW',NULL,NULL,NULL),
(19,'BMW Alpina',NULL,NULL,NULL),
(20,'Brabus',NULL,NULL,NULL),
(21,'Brilliance',NULL,NULL,NULL),
(22,'Bristol',NULL,NULL,NULL),
(23,'Bufori',NULL,NULL,NULL),
(24,'Bugatti',NULL,NULL,NULL),
(25,'Buick',NULL,NULL,NULL),
(26,'BYD',NULL,NULL,NULL),
(27,'Byvin',NULL,NULL,NULL),
(28,'Cadillac',NULL,NULL,NULL),
(29,'Callaway',NULL,NULL,NULL),
(30,'Carbodies',NULL,NULL,NULL),
(31,'Caterham',NULL,NULL,NULL),
(32,'Changan',NULL,NULL,NULL),
(33,'ChangFeng',NULL,NULL,NULL),
(34,'Chery',NULL,NULL,NULL),
(35,'Chevrolet',NULL,NULL,NULL),
(36,'Chrysler',NULL,NULL,NULL),
(37,'Citroen',NULL,NULL,NULL),
(38,'Cizeta',NULL,NULL,NULL),
(39,'Coggiola',NULL,NULL,NULL),
(40,'Dacia',NULL,NULL,NULL),
(41,'Dadi',NULL,NULL,NULL),
(42,'Daewoo',NULL,NULL,NULL),
(43,'DAF',NULL,NULL,NULL),
(44,'Daihatsu',NULL,NULL,NULL),
(45,'Daimler',NULL,NULL,NULL),
(46,'Dallas',NULL,NULL,NULL),
(47,'Datsun',NULL,NULL,NULL),
(48,'De Tomaso',NULL,NULL,NULL),
(49,'DeLorean',NULL,NULL,NULL),
(50,'Derways',NULL,NULL,NULL),
(51,'Dodge',NULL,NULL,NULL),
(52,'DongFeng',NULL,NULL,NULL),
(53,'Doninvest',NULL,NULL,NULL),
(54,'Donkervoort',NULL,NULL,NULL),
(55,'E-Car',NULL,NULL,NULL),
(56,'Eagle',NULL,NULL,NULL),
(57,'Eagle Cars',NULL,NULL,NULL),
(58,'Ecomotors',NULL,NULL,NULL),
(59,'FAW',NULL,NULL,NULL),
(60,'Ferrari',NULL,NULL,NULL),
(61,'Fiat',NULL,NULL,NULL),
(62,'Fisker',NULL,NULL,NULL),
(63,'Ford',NULL,NULL,NULL),
(64,'Foton',NULL,NULL,NULL),
(65,'FSO',NULL,NULL,NULL),
(66,'Fuqi',NULL,NULL,NULL),
(67,'Geely',NULL,NULL,NULL),
(68,'Geo',NULL,NULL,NULL),
(69,'GMC',NULL,NULL,NULL),
(70,'Gonow',NULL,NULL,NULL),
(71,'Great Wall',NULL,NULL,NULL),
(72,'Hafei',NULL,NULL,NULL),
(73,'Haima',NULL,NULL,NULL),
(74,'Hindustan',NULL,NULL,NULL),
(75,'Holden',NULL,NULL,NULL),
(76,'Honda',NULL,NULL,NULL),
(77,'HuangHai',NULL,NULL,NULL),
(78,'Hummer',NULL,NULL,NULL),
(79,'Hyundai',NULL,NULL,NULL),
(80,'Infiniti',NULL,NULL,NULL),
(81,'Innocenti',NULL,NULL,NULL),
(82,'Invicta',NULL,NULL,NULL),
(83,'Iran Khodro',NULL,NULL,NULL),
(84,'Isdera',NULL,NULL,NULL),
(85,'Isuzu',NULL,NULL,NULL),
(86,'IVECO',NULL,NULL,NULL),
(87,'JAC',NULL,NULL,NULL),
(88,'Jaguar',NULL,NULL,NULL),
(89,'Jeep',NULL,NULL,NULL),
(90,'Jensen',NULL,NULL,NULL),
(91,'JMC',NULL,NULL,NULL),
(92,'Kia',NULL,NULL,NULL),
(93,'Koenigsegg',NULL,NULL,NULL),
(94,'KTM',NULL,NULL,NULL),
(95,'Lamborghini',NULL,NULL,NULL),
(96,'Lancia',NULL,NULL,NULL),
(97,'Land Rover',NULL,NULL,NULL),
(98,'Landwind',NULL,NULL,NULL),
(99,'Lexus',NULL,NULL,NULL),
(100,'Liebao Motor',NULL,NULL,NULL),
(101,'Lifan',NULL,NULL,NULL),
(102,'Lincoln',NULL,NULL,NULL),
(103,'Lotus',NULL,NULL,NULL),
(104,'LTI',NULL,NULL,NULL),
(105,'Luxgen',NULL,NULL,NULL),
(106,'Mahindra',NULL,NULL,NULL),
(107,'Marcos',NULL,NULL,NULL),
(108,'Marlin',NULL,NULL,NULL),
(109,'Marussia',NULL,NULL,NULL),
(110,'Maruti',NULL,NULL,NULL),
(111,'Maserati',NULL,NULL,NULL),
(112,'Maybach',NULL,NULL,NULL),
(113,'Mazda',NULL,NULL,NULL),
(114,'McLaren',NULL,NULL,NULL),
(115,'Mega',NULL,NULL,NULL),
(116,'Mercedes-Benz',NULL,NULL,NULL),
(117,'Mercury',NULL,NULL,NULL),
(118,'Metrocab',NULL,NULL,NULL),
(119,'MG',NULL,NULL,NULL),
(120,'Microcar',NULL,NULL,NULL),
(121,'Minelli',NULL,NULL,NULL),
(122,'Mini',NULL,NULL,NULL),
(123,'Mitsubishi',NULL,NULL,NULL),
(124,'Mitsuoka',NULL,NULL,NULL),
(125,'Morgan',NULL,NULL,NULL),
(126,'Morris',NULL,NULL,NULL),
(127,'Nissan',NULL,NULL,NULL),
(128,'Noble',NULL,NULL,NULL),
(129,'Oldsmobile',NULL,NULL,NULL),
(130,'Opel',NULL,NULL,NULL),
(131,'Osca',NULL,NULL,NULL),
(132,'Pagani',NULL,NULL,NULL),
(133,'Panoz',NULL,NULL,NULL),
(134,'Perodua',NULL,NULL,NULL),
(135,'Peugeot',NULL,NULL,NULL),
(136,'Piaggio',NULL,NULL,NULL),
(137,'Plymouth',NULL,NULL,NULL),
(138,'Pontiac',NULL,NULL,NULL),
(139,'Porsche',NULL,NULL,NULL),
(140,'Premier',NULL,NULL,NULL),
(141,'Proton',NULL,NULL,NULL),
(142,'PUCH',NULL,NULL,NULL),
(143,'Puma',NULL,NULL,NULL),
(144,'Qoros',NULL,NULL,NULL),
(145,'Qvale',NULL,NULL,NULL),
(146,'Reliant',NULL,NULL,NULL),
(147,'Renault',NULL,NULL,NULL),
(148,'Renault Samsung',NULL,NULL,NULL),
(149,'Rolls-Royce',NULL,NULL,NULL),
(150,'Ronart',NULL,NULL,NULL),
(151,'Rover',NULL,NULL,NULL),
(152,'Saab',NULL,NULL,NULL),
(153,'Saleen',NULL,NULL,NULL),
(154,'Santana',NULL,NULL,NULL),
(155,'Saturn',NULL,NULL,NULL),
(156,'Scion',NULL,NULL,NULL),
(157,'SEAT',NULL,NULL,NULL),
(158,'ShuangHuan',NULL,NULL,NULL),
(159,'Skoda',NULL,NULL,NULL),
(160,'Smart',NULL,NULL,NULL),
(161,'Soueast',NULL,NULL,NULL),
(162,'Spectre',NULL,NULL,NULL),
(163,'Spyker',NULL,NULL,NULL),
(165,'Ssang Yong',NULL,NULL,NULL),
(166,'Subaru',NULL,NULL,NULL),
(167,'Suzuki',NULL,NULL,NULL),
(168,'Talbot',NULL,NULL,NULL),
(169,'TATA',NULL,NULL,NULL),
(170,'Tatra',NULL,NULL,NULL),
(171,'Tazzari',NULL,NULL,NULL),
(172,'Tesla',NULL,NULL,NULL),
(173,'Tianma',NULL,NULL,NULL),
(174,'Tianye',NULL,NULL,NULL),
(175,'Tofas',NULL,NULL,NULL),
(176,'Toyota',NULL,NULL,NULL),
(177,'Trabant',NULL,NULL,NULL),
(178,'Tramontana',NULL,NULL,NULL),
(179,'Triumph',NULL,NULL,NULL),
(180,'TVR',NULL,NULL,NULL),
(181,'Vauxhall',NULL,NULL,NULL),
(182,'Vector',NULL,NULL,NULL),
(183,'Venturi',NULL,NULL,NULL),
(184,'Volkswagen',NULL,NULL,NULL),
(185,'Volvo',NULL,NULL,NULL),
(186,'Vortex',NULL,NULL,NULL),
(187,'Wartburg',NULL,NULL,NULL),
(188,'Westfield',NULL,NULL,NULL),
(189,'Wiesmann',NULL,NULL,NULL),
(190,'Xin Kai',NULL,NULL,NULL),
(191,'Zastava',NULL,NULL,NULL),
(192,'Zotye',NULL,NULL,NULL),
(193,'ZX',NULL,NULL,NULL),
(211,'Ё-мобиль',NULL,NULL,NULL),
(212,'Автокам',NULL,NULL,NULL),
(213,'Астро',NULL,NULL,NULL),
(214,'Бронто',NULL,NULL,NULL),
(215,'ВАЗ',NULL,NULL,NULL),
(216,'ГАЗ',NULL,NULL,NULL),
(217,'ЗАЗ',NULL,NULL,NULL),
(218,'ЗИЛ',NULL,NULL,NULL),
(219,'ИЖ',NULL,NULL,NULL),
(220,'КамАЗ',NULL,NULL,NULL),
(221,'Канонир',NULL,NULL,NULL),
(222,'ЛУАЗ',NULL,NULL,NULL),
(223,'Москвич',NULL,NULL,NULL),
(224,'СМЗ',NULL,NULL,NULL),
(225,'СеАЗ',NULL,NULL,NULL),
(226,'ТагАЗ',NULL,NULL,NULL),
(227,'УАЗ',NULL,NULL,NULL),
(280,'Ultima',NULL,NULL,NULL),
(282,'Hawtai',NULL,NULL,NULL),
(284,'Renaissance',NULL,NULL,NULL),
(285,'Все марки',NULL,NULL,NULL);

UNLOCK TABLES;

/*Table structure for table `car_model` */

CREATE TABLE `car_model` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(300) NOT NULL,
  `car_manufacturer_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `car_manufacturer_id` (`car_manufacturer_id`),
  CONSTRAINT `car_model_ibfk_1` FOREIGN KEY (`car_manufacturer_id`) REFERENCES `car_manufacturer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2273 DEFAULT CHARSET=utf8;

/*Data for the table `car_model` */

LOCK TABLES `car_model` WRITE;

insert  into `car_model`(`id`,`title`,`car_manufacturer_id`) values 
(1,'378 GT Zagato',1),
(2,'Ace',1),
(3,'Aceca',1),
(4,'Cobra',1),
(5,'CL',2),
(6,'CSX',2),
(7,'EL',2),
(8,'ILX',2),
(9,'Integra',2),
(10,'Legend',2),
(11,'MDX',2),
(12,'NSX',2),
(13,'RDX',2),
(14,'RL',2),
(15,'RLX',2),
(16,'RSX',2),
(17,'SLX',2),
(18,'TL',2),
(19,'TSX',2),
(20,'ZDX',2),
(21,'145',3),
(22,'146',3),
(23,'147',3),
(24,'155',3),
(25,'156',3),
(26,'159',3),
(27,'164',3),
(28,'166',3),
(29,'33',3),
(30,'4C',3),
(31,'6',3),
(32,'75',3),
(33,'8C Competizione',3),
(34,'90',3),
(35,'Alfasud',3),
(36,'Alfetta',3),
(37,'Arna',3),
(38,'Brera',3),
(39,'Giulia',3),
(40,'Giulietta',3),
(41,'GT',3),
(42,'GTA Coupe',3),
(43,'GTV',3),
(44,'MiTo',3),
(45,'Montreal',3),
(46,'RZ',3),
(47,'Spider',3),
(48,'Sprint',3),
(49,'SZ',3),
(50,'A110',4),
(51,'A310',4),
(52,'A610',4),
(53,'GTA',4),
(54,'HMMWV (Humvee)',5),
(55,'Atom',6),
(56,'10',7),
(57,'24',7),
(58,'Retona',8),
(59,'Rocsta',8),
(60,'Bulldog',9),
(61,'Cygnet',9),
(62,'DB7',9),
(63,'DB9',9),
(64,'DBS',9),
(65,'Lagonda',9),
(66,'One-77',9),
(67,'Rapide',9),
(68,'Tickford Capri',9),
(69,'V12 Vanquish',9),
(70,'V12 Vantage',9),
(71,'V12 Zagato',9),
(72,'V8 Vantage',9),
(73,'V8 Zagato',9),
(74,'Virage',9),
(75,'100',10),
(76,'200',10),
(77,'50',10),
(78,'80',10),
(79,'90',10),
(80,'A1',10),
(81,'A2',10),
(82,'A3',10),
(83,'A4 allroad',10),
(84,'A4',10),
(85,'A5',10),
(86,'A6 allroad',10),
(87,'A6',10),
(88,'A7',10),
(89,'A8',10),
(90,'Cabriolet',10),
(91,'Coupe',10),
(92,'NSU RO 80',10),
(93,'Q3',10),
(94,'Q5',10),
(95,'Q7',10),
(96,'quattro',10),
(97,'R8',10),
(98,'RS Q3',10),
(99,'RS2',10),
(100,'RS3',10),
(101,'RS4',10),
(102,'RS5',10),
(103,'RS6',10),
(104,'RS7',10),
(105,'S1',10),
(106,'S2',10),
(107,'S3',10),
(108,'S4',10),
(109,'S5',10),
(110,'S6',10),
(111,'S7',10),
(112,'S8',10),
(113,'SQ5',10),
(114,'TT',10),
(115,'V8',10),
(116,'Allegro',11),
(117,'Ambassador',11),
(118,'Maestro',11),
(119,'Maxi',11),
(120,'Metro',11),
(121,'Mini',11),
(122,'Montego',11),
(123,'Princess',11),
(124,'A 112',12),
(125,'BD-1322',13),
(126,'BJ2020',14),
(127,'BJ212',14),
(128,'Arnage',15),
(129,'Azure',15),
(130,'Brooklands',15),
(131,'Continental',15),
(132,'Continental Flying Spur',15),
(133,'Continental GT',15),
(134,'Eight',15),
(135,'Flying Spur',15),
(136,'Mulsanne',15),
(137,'Turbo R',15),
(138,'Freeclimber',16),
(139,'Type 3',17),
(140,'02 (E10)',18),
(141,'1er',18),
(142,'2er',18),
(143,'2er Active Tourer',18),
(144,'3er',18),
(145,'4er',18),
(146,'5er',18),
(147,'6er',18),
(148,'7er',18),
(149,'8er',18),
(150,'i3',18),
(151,'i8',18),
(152,'X1',18),
(153,'X3',18),
(154,'X4',18),
(155,'X5',18),
(156,'X6',18),
(157,'Z1',18),
(158,'Z3',18),
(159,'Z4',18),
(160,'Z8',18),
(161,'3er',19),
(162,'4er',19),
(163,'5er',19),
(164,'6er',19),
(165,'7er',19),
(166,'8er',19),
(167,'Roadster',19),
(168,'XD3',19),
(169,'7.3S',20),
(170,'M V12',20),
(171,'SV12',20),
(172,'FRV (BS2)',21),
(173,'M1 (BS6)',21),
(174,'M2 (BS4)',21),
(175,'M3 (BC3)',21),
(176,'V5',21),
(177,'Blenheim',22),
(178,'Blenheim Speedster',22),
(179,'Fighter',22),
(180,'Geneva',23),
(181,'La Joya',23),
(182,'EB 110',24),
(183,'EB 112',24),
(184,'EB 16.4 Veyron',24),
(185,'Century',25),
(186,'Electra',25),
(187,'Enclave',25),
(188,'Excelle',25),
(189,'GL8',25),
(190,'LaCrosse',25),
(191,'LeSabre',25),
(192,'Lucerne',25),
(193,'Park Avenue',25),
(194,'Rainer',25),
(195,'Reatta',25),
(196,'Regal',25),
(197,'Rendezvous',25),
(198,'Riviera',25),
(199,'Roadmaster',25),
(200,'Skylark',25),
(201,'Terraza',25),
(202,'E6',26),
(203,'F0',26),
(204,'F3',26),
(205,'F6',26),
(206,'F8',26),
(207,'Flyer',26),
(208,'G3',26),
(209,'G6',26),
(210,'L3',26),
(211,'M6',26),
(212,'S6',26),
(213,'BD132J (CoCo)',27),
(214,'BD326J (Moca)',27),
(215,'Allante',28),
(216,'ATS',28),
(217,'BLS',28),
(218,'Brougham',28),
(219,'Catera',28),
(220,'CTS',28),
(221,'De Ville',28),
(222,'DTS',28),
(223,'Eldorado',28),
(224,'Escalade',28),
(225,'Fleetwood',28),
(226,'LSE',28),
(227,'Seville',28),
(228,'Sixty Special',28),
(229,'SRX',28),
(230,'STS',28),
(231,'XLR',28),
(232,'XTS',28),
(233,'C12',29),
(234,'FX4',30),
(235,'21',31),
(236,'CSR',31),
(237,'Seven',31),
(238,'Benni',32),
(239,'CS35',32),
(240,'Eado',32),
(241,'Raeton',32),
(242,'Z-Shine',32),
(244,'Flying',33),
(245,'SUV (CS6)',33),
(247,'Amulet (A15)',34),
(248,'Bonus (A13)',34),
(249,'CrossEastar (B14)',34),
(250,'Fora (A21)',34),
(251,'IndiS (S18D)',34),
(252,'Kimo (A1)',34),
(253,'Oriental Son (B11)',34),
(254,'QQ6 (S21)',34),
(255,'Sweet (QQ)',34),
(256,'Tiggo (T11)',34),
(257,'Very',34),
(258,'Alero',35),
(259,'Astra',35),
(260,'Astro',35),
(261,'Avalanche',35),
(262,'Aveo',35),
(263,'Beretta',35),
(264,'Blazer',35),
(265,'Blazer K5',35),
(266,'C-10',35),
(267,'Camaro',35),
(268,'Caprice',35),
(269,'Captiva',35),
(270,'Cavalier',35),
(271,'Celebrity',35),
(272,'Celta',35),
(273,'Chevette',35),
(274,'Citation',35),
(275,'Classic',35),
(276,'Cobalt',35),
(277,'Colorado',35),
(278,'Corsa',35),
(279,'Corsica',35),
(280,'Corvette',35),
(281,'Cruze (HR)',35),
(282,'Cruze',35),
(283,'Epica',35),
(284,'Equinox',35),
(285,'Evanda',35),
(286,'Express',35),
(287,'HHR',35),
(288,'Impala',35),
(289,'Kalos',35),
(290,'Lacetti',35),
(291,'Lanos',35),
(292,'Lumina',35),
(293,'Lumina APV',35),
(294,'LUV D-MAX',35),
(295,'Malibu',35),
(296,'Metro',35),
(297,'Monte Carlo',35),
(298,'Monza',35),
(299,'MW',35),
(300,'Niva',35),
(301,'Nubira',35),
(302,'Omega',35),
(303,'Orlando',35),
(304,'Prizm',35),
(305,'Rezzo',35),
(306,'S-10 Pickup',35),
(307,'Sail',35),
(308,'Sonic',35),
(309,'Spark',35),
(310,'SSR',35),
(311,'Starcraft',35),
(312,'Suburban',35),
(313,'Tahoe',35),
(314,'Tavera',35),
(315,'Tracker',35),
(316,'TrailBlazer',35),
(317,'Trans Sport',35),
(318,'200',36),
(319,'300C',36),
(320,'300M',36),
(321,'Aspen',36),
(322,'Cirrus',36),
(323,'Concorde',36),
(324,'Crossfire',36),
(325,'Dynasty',36),
(326,'Fifth Avenue',36),
(327,'Imperial',36),
(328,'Intrepid',36),
(329,'Le Baron',36),
(330,'LHS',36),
(331,'Nassau',36),
(332,'Neon',36),
(333,'NEW Yorker',36),
(334,'Pacifica',36),
(335,'Prowler',36),
(336,'PT Cruiser',36),
(337,'Saratoga',36),
(338,'Sebring',36),
(339,'Stratus',36),
(340,'TC by Maserati',36),
(341,'Town & Country',36),
(342,'Viper',36),
(343,'Vision',36),
(344,'Voyager',36),
(345,'2 CV',37),
(346,'AMI',37),
(347,'Ax',37),
(348,'Berlingo',37),
(349,'BX',37),
(350,'C-Crosser',37),
(351,'C-Elysee',37),
(352,'C1',37),
(353,'C2',37),
(354,'C3',37),
(355,'C3 Picasso',37),
(356,'C4 Aircross',37),
(357,'C4 Cactus',37),
(358,'C4',37),
(359,'C4 Picasso',37),
(360,'C5',37),
(361,'C6',37),
(362,'C8',37),
(363,'CX',37),
(364,'DS3',37),
(365,'DS4',37),
(366,'DS5',37),
(367,'Dyane',37),
(368,'Evasion',37),
(369,'GS',37),
(370,'LNA',37),
(371,'Saxo',37),
(372,'Visa',37),
(373,'Xantia',37),
(374,'XM',37),
(375,'Xsara',37),
(376,'Xsara Picasso',37),
(377,'ZX',37),
(378,'V16t',38),
(379,'T Rex',39),
(380,'1300',40),
(381,'1310',40),
(382,'1410',40),
(383,'Dokker',40),
(384,'Duster',40),
(385,'Lodgy',40),
(386,'Logan',40),
(387,'Nova',40),
(388,'Sandero',40),
(389,'Solenza',40),
(390,'City Leading',41),
(391,'Shuttle',41),
(392,'Smoothing',41),
(393,'Arcadia',42),
(394,'Chairman',42),
(395,'Damas',42),
(396,'Espero',42),
(397,'Evanda',42),
(398,'G2X',42),
(399,'Gentra',42),
(400,'Kalos',42),
(401,'Korando',42),
(402,'Lacetti',42),
(403,'Lanos (Sens)',42),
(404,'LE Mans',42),
(405,'Leganza',42),
(406,'Magnus',42),
(407,'Matiz',42),
(408,'Musso',42),
(409,'Nexia',42),
(410,'Nubira',42),
(411,'Prince',42),
(412,'Racer',42),
(413,'Rezzo',42),
(414,'Tacuma',42),
(415,'Tico',42),
(416,'Tosca',42),
(417,'Winstorm',42),
(418,'46',43),
(419,'66',43),
(420,'Altis',44),
(421,'Applause',44),
(422,'Atrai',44),
(423,'Be-go',44),
(424,'Boon',44),
(425,'Ceria',44),
(426,'Charade',44),
(427,'Charmant',44),
(428,'Coo',44),
(429,'Copen',44),
(430,'Cuore',44),
(431,'Delta Wagon',44),
(432,'Esse',44),
(433,'Feroza',44),
(434,'Gran Move',44),
(435,'Leeza',44),
(436,'Materia',44),
(437,'MAX',44),
(438,'Mira Gino',44),
(439,'Mira',44),
(440,'Move',44),
(441,'Naked',44),
(442,'Opti',44),
(443,'Pyzar',44),
(444,'Rocky',44),
(445,'Sirion',44),
(446,'Sonica',44),
(447,'Storia',44),
(448,'Taft',44),
(449,'Tanto',44),
(450,'Terios',44),
(451,'Trevis',44),
(452,'Wildcat',44),
(453,'Xenia',44),
(454,'YRV',44),
(455,'DS420',45),
(456,'Sovereign (XJ6)',45),
(457,'X300',45),
(458,'X308',45),
(459,'X350',45),
(460,'XJ40',45),
(461,'XJS',45),
(462,'Jeep',46),
(463,'280ZX',47),
(464,'720',47),
(465,'Bluebird',47),
(466,'Cherry',47),
(467,'GO',47),
(468,'GO+',47),
(469,'on-DO',47),
(470,'Stanza',47),
(471,'Sunny',47),
(472,'Violet',47),
(473,'Bigua',48),
(474,'Guara',48),
(475,'Mangusta',48),
(476,'Pantera',48),
(477,'Vallelunga',48),
(478,'DMC-12',49),
(479,'Antelope',50),
(480,'Aurora',50),
(481,'Cowboy',50),
(482,'Land Crown',50),
(483,'Plutus',50),
(484,'Saladin',50),
(485,'Shuttle',50),
(486,'600',51),
(487,'Aries',51),
(488,'Avenger',51),
(489,'Caliber',51),
(490,'Caravan',51),
(491,'Challenger',51),
(492,'Charger',51),
(493,'Dakota',51),
(494,'Dart',51),
(495,'Daytona',51),
(496,'Durango',51),
(497,'Dynasty',51),
(498,'Grand Caravan',51),
(499,'Intrepid',51),
(500,'Journey',51),
(501,'Magnum',51),
(502,'Monaco',51),
(503,'Neon',51),
(504,'Nitro',51),
(505,'Omni',51),
(506,'RAM',51),
(507,'Ramcharger',51),
(508,'Shadow',51),
(509,'Spirit',51),
(510,'Stealth',51),
(511,'Stratus',51),
(512,'Viper',51),
(513,'H30 Cross',52),
(514,'MPV',52),
(515,'Oting',52),
(516,'Rich',52),
(517,'Assol',53),
(518,'Kondor',53),
(519,'Orion',53),
(520,'D8',54),
(521,'GD04B',55),
(522,'Premier',56),
(523,'Summit',56),
(524,'Talon',56),
(525,'Vision',56),
(526,'SS',57),
(527,'Estrima Biro',58),
(528,'A6',59),
(529,'Audi 100',59),
(530,'Besturn B50',59),
(531,'Besturn B70',59),
(532,'Bora',59),
(533,'City Golf',59),
(534,'Jetta',59),
(535,'Jinn',59),
(536,'Oley',59),
(537,'V2',59),
(538,'V5',59),
(539,'Vita',59),
(540,'208/308',60),
(541,'328',60),
(542,'348',60),
(543,'360',60),
(544,'400',60),
(545,'412',60),
(546,'456',60),
(547,'458',60),
(548,'512 BB',60),
(549,'512 M',60),
(550,'512 TR',60),
(551,'550',60),
(552,'575M',60),
(553,'599',60),
(554,'612',60),
(555,'California',60),
(556,'Enzo',60),
(557,'F12berlinetta',60),
(558,'F355',60),
(559,'F40',60),
(560,'F430',60),
(561,'F50',60),
(562,'FF',60),
(563,'LaFerrari',60),
(564,'Mondial',60),
(565,'Testarossa',60),
(566,'124',61),
(567,'126',61),
(568,'127',61),
(569,'128',61),
(570,'130',61),
(571,'131',61),
(572,'132',61),
(573,'238',61),
(574,'500',61),
(575,'500L',61),
(576,'600',61),
(577,'900T',61),
(578,'Albea',61),
(579,'Argenta',61),
(580,'Barchetta',61),
(581,'Brava',61),
(582,'Bravo',61),
(583,'Cinquecento',61),
(584,'Coupe',61),
(585,'Croma',61),
(586,'Doblo',61),
(587,'Duna',61),
(588,'Fiorino',61),
(589,'Freemont',61),
(590,'Idea',61),
(591,'Linea',61),
(592,'Marea',61),
(593,'Multipla',61),
(594,'Palio',61),
(595,'Panda',61),
(596,'Punto',61),
(597,'Qubo',61),
(598,'Regata',61),
(599,'Ritmo',61),
(600,'Sedici',61),
(601,'Seicento',61),
(602,'Siena',61),
(603,'Stilo',61),
(604,'Strada',61),
(605,'Tempra',61),
(606,'Tipo',61),
(607,'Ulysse',61),
(608,'UNO',61),
(609,'X 1/9',61),
(610,'Karma',62),
(611,'Aerostar',63),
(612,'Aspire',63),
(613,'B-MAX',63),
(614,'Bronco',63),
(615,'C-MAX',63),
(616,'Capri',63),
(617,'Consul',63),
(618,'Contour',63),
(619,'Cougar',63),
(620,'Crown Victoria',63),
(621,'Econoline',63),
(622,'EcoSport',63),
(623,'Edge',63),
(624,'Escape',63),
(625,'Escort (North America)',63),
(626,'Escort',63),
(627,'Everest',63),
(628,'Excursion',63),
(629,'Expedition',63),
(630,'Explorer',63),
(631,'F-150',63),
(632,'Fairmont',63),
(633,'Festiva',63),
(634,'Fiesta',63),
(635,'Five Hundred',63),
(636,'Flex',63),
(637,'Focus',63),
(638,'Freestar',63),
(639,'Freestyle',63),
(640,'Fusion (North America)',63),
(641,'Fusion',63),
(642,'Galaxy',63),
(643,'Granada (North America)',63),
(644,'Granada',63),
(645,'GT',63),
(646,'Ixion',63),
(647,'KA',63),
(648,'Kuga',63),
(649,'Laser',63),
(650,'LTD Crown Victoria',63),
(651,'Maverick',63),
(652,'Mondeo',63),
(653,'Mustang',63),
(654,'Orion',63),
(655,'Probe',63),
(656,'Puma',63),
(657,'Ranger (North America)',63),
(658,'Ranger',63),
(659,'S-MAX',63),
(660,'Scorpio',63),
(661,'Sierra',63),
(662,'Sport Trac',63),
(663,'Taunus',63),
(664,'Taurus',63),
(665,'Taurus X',63),
(666,'Tempo',63),
(667,'Thunderbird',63),
(668,'Tourneo Connect',63),
(669,'Windstar',63),
(670,'Midi',64),
(671,'Tunland',64),
(672,'125p',65),
(673,'126p',65),
(674,'127p',65),
(675,'132p',65),
(676,'Polonez',65),
(677,'6500 (Land King)',66),
(678,'Beauty Leopard',67),
(679,'CK (Otaka)',67),
(680,'Emgrand EC7',67),
(681,'Emgrand EC8',67),
(682,'Emgrand X7',67),
(683,'FC (Vision)',67),
(684,'Haoqing',67),
(685,'LC (Panda)',67),
(686,'LC (Panda) Cross',67),
(687,'MK Cross',67),
(688,'MK',67),
(689,'MR',67),
(690,'Metro',68),
(691,'Prizm',68),
(692,'Spectrum',68),
(693,'Storm',68),
(694,'Tracker',68),
(695,'Acadia',69),
(696,'Canyon',69),
(697,'Envoy',69),
(698,'Jimmy',69),
(699,'Safari',69),
(700,'Savana',69),
(701,'Sierra',69),
(702,'Sonoma',69),
(703,'Suburban',69),
(704,'Syclone',69),
(705,'Terrain',69),
(706,'Typhoon',69),
(707,'Vandura',69),
(708,'Yukon',69),
(709,'Troy',70),
(710,'Coolbear',71),
(711,'Cowry (V80)',71),
(712,'Deer',71),
(713,'Florid',71),
(714,'Hover',71),
(715,'Hover M1 (Peri 4x4)',71),
(716,'Hover M2',71),
(717,'Hover M4',71),
(718,'Pegasus',71),
(719,'Peri',71),
(720,'Safe',71),
(721,'Sailor',71),
(722,'Sing RUV',71),
(723,'Socool',71),
(724,'Voleex C10 (Phenom)',71),
(725,'Wingle',71),
(726,'Brio',72),
(727,'Princip',72),
(728,'Saibao',72),
(729,'Sigma',72),
(730,'Simbo',72),
(731,'3',73),
(732,'7',73),
(733,'M3',73),
(734,'Ambassador',74),
(735,'Contessa',74),
(736,'Apollo',75),
(737,'Astra',75),
(738,'Barina',75),
(739,'Calais',75),
(740,'Caprice',75),
(741,'Combo',75),
(742,'Commodore',75),
(743,'Cruze',75),
(744,'Frontera',75),
(745,'Jackaroo',75),
(746,'Monaro',75),
(747,'Rodeo',75),
(748,'Statesmann',75),
(749,'Suburban',75),
(750,'UTE',75),
(751,'Vectra',75),
(752,'Zafira',75),
(753,'Accord',76),
(754,'Airwave',76),
(755,'Ascot',76),
(756,'Avancier',76),
(757,'Beat',76),
(758,'Capa',76),
(759,'City',76),
(760,'Civic Ferio',76),
(761,'Civic',76),
(762,'Concerto',76),
(763,'CR-V',76),
(764,'CR-X',76),
(765,'CR-Z',76),
(766,'Crossroad',76),
(767,'Crosstour',76),
(768,'Domani',76),
(769,'Edix',76),
(770,'Element',76),
(771,'Elysion',76),
(772,'FCX Clarity',76),
(773,'Fit Aria',76),
(774,'Fit',76),
(775,'FR-V',76),
(776,'Freed',76),
(777,'HR-V',76),
(778,'Insight',76),
(779,'Inspire',76),
(780,'Integra',76),
(781,'Integra SJ',76),
(782,'Jazz',76),
(783,'Legend',76),
(784,'Life',76),
(785,'Logo',76),
(786,'MDX',76),
(787,'Mobilio',76),
(788,'NSX',76),
(789,'Odyssey (North America)',76),
(790,'Odyssey',76),
(791,'Orthia',76),
(792,'Partner',76),
(793,'Passport',76),
(794,'Pilot',76),
(795,'Prelude',76),
(796,'Quint',76),
(797,'Rafaga',76),
(798,'Ridgeline',76),
(799,'S-MX',76),
(800,'S2000',76),
(801,'Saber',76),
(802,'Shuttle',76),
(803,'Stepwgn',76),
(804,'Stream',76),
(805,'ThatS',76),
(806,'Today',76),
(807,'Torneo',76),
(808,'Vamos',76),
(809,'Vigor',76),
(810,'Z',76),
(811,'Zest',76),
(812,'Antelope',77),
(813,'Landscape',77),
(814,'Plutus',77),
(815,'H1',78),
(816,'H2',78),
(817,'H3',78),
(818,'Accent',79),
(819,'Atos',79),
(820,'Avante',79),
(821,'Centennial',79),
(822,'Coupe',79),
(823,'Dynasty',79),
(824,'Elantra',79),
(825,'Equus',79),
(826,'Excel',79),
(827,'Galloper',79),
(828,'Genesis',79),
(829,'Genesis Coupe',79),
(830,'Getz',79),
(831,'Grandeur',79),
(832,'i10',79),
(833,'i20',79),
(834,'i30',79),
(835,'i40',79),
(836,'ix20',79),
(837,'ix35',79),
(838,'ix55',79),
(839,'Lantra',79),
(840,'Lavita',79),
(841,'Marcia',79),
(842,'Matrix',79),
(843,'Maxcruz',79),
(844,'Pony',79),
(845,'Santa Fe',79),
(846,'Santamo',79),
(847,'Scoupe',79),
(848,'Solaris',79),
(849,'Sonata',79),
(850,'Starex (H-1)',79),
(851,'Stellar',79),
(852,'Terracan',79),
(853,'Tiburon',79),
(854,'Trajet',79),
(855,'Tucson',79),
(856,'Tuscani',79),
(857,'Veloster',79),
(858,'Veracruz',79),
(859,'Verna',79),
(860,'XG',79),
(861,'EX',80),
(862,'FX',80),
(863,'G',80),
(864,'I',80),
(865,'J',80),
(866,'JX',80),
(867,'M',80),
(868,'Q',80),
(869,'Q50',80),
(870,'Q60',80),
(871,'QX',80),
(872,'QX50',80),
(873,'QX60',80),
(874,'QX70',80),
(875,'QX80',80),
(876,'Elba',81),
(877,'Mille',81),
(878,'Mini',81),
(879,'S1',82),
(880,'Paykan',83),
(881,'Samand',83),
(882,'Soren',83),
(883,'Commendatore 112i',84),
(884,'Imperator 108i',84),
(885,'Spyder',84),
(886,'Amigo',85),
(887,'Ascender',85),
(888,'Aska',85),
(889,'Axiom',85),
(890,'Bighorn',85),
(891,'D-Max',85),
(892,'Gemini',85),
(893,'Impulse',85),
(894,'KB',85),
(895,'Mu',85),
(896,'MU-7',85),
(897,'Piazza',85),
(898,'Rodeo',85),
(899,'Stylus',85),
(900,'TF (Pickup)',85),
(901,'Trooper',85),
(902,'VehiCross',85),
(903,'Wizard',85),
(904,'Massif',86),
(905,'J2 (Yueyue)',87),
(906,'J3 (Tongyue,Tojoy)',87),
(907,'J5 (Heyue)',87),
(908,'J6 (Heyue RS)',87),
(909,'J7 (Binyue)',87),
(910,'Refine',87),
(911,'S1 (Rein)',87),
(912,'S5 (Eagle)',87),
(913,'E-type 2+2',88),
(914,'E-type',88),
(915,'F-Type',88),
(916,'S-Type',88),
(917,'X-Type',88),
(918,'XF',88),
(919,'XJ',88),
(920,'XJ220',88),
(921,'XJS',88),
(922,'XK',88),
(923,'Cherokee',89),
(924,'CJ',89),
(925,'Commander',89),
(926,'Compass',89),
(927,'Grand Cherokee',89),
(928,'Grand Wagoneer',89),
(929,'Liberty (North America)',89),
(930,'Liberty (Patriot)',89),
(931,'Wrangler',89),
(932,'S-V8',90),
(933,'Baodian',91),
(934,'Avella',92),
(935,'Cadenza',92),
(936,'Capital',92),
(937,'Carens',92),
(938,'Carnival',92),
(939,'Ceed',92),
(940,'Cerato',92),
(941,'Clarus',92),
(942,'Concord',92),
(943,'Elan',92),
(944,'Enterprise',92),
(945,'Joice',92),
(946,'Magentis',92),
(947,'Mohave (Borrego)',92),
(948,'Opirus',92),
(949,'Optima',92),
(950,'Picanto',92),
(951,'Potentia',92),
(952,'Pride',92),
(953,'Quoris',92),
(954,'Ray',92),
(955,'Retona',92),
(956,'Rio',92),
(957,'Sedona',92),
(958,'Sephia',92),
(959,'Shuma',92),
(960,'Sorento',92),
(961,'Soul',92),
(962,'Spectra',92),
(963,'Sportage',92),
(964,'Venga',92),
(965,'Visto',92),
(966,'X-Trek',92),
(967,'Agera',93),
(968,'CC8S',93),
(969,'CCR',93),
(970,'CCX',93),
(971,'One:1',93),
(972,'X-Bow',94),
(973,'Aventador',95),
(974,'Countach',95),
(975,'Diablo',95),
(976,'Espada',95),
(977,'Gallardo',95),
(978,'Huracan',95),
(979,'Jalpa',95),
(980,'Jarama',95),
(981,'LM001',95),
(982,'LM002',95),
(983,'Murcielago',95),
(984,'Reventon',95),
(985,'Urraco',95),
(986,'Veneno',95),
(987,'A 112',96),
(988,'Beta',96),
(989,'Dedra',96),
(990,'Delta',96),
(991,'Fulvia',96),
(992,'Gamma',96),
(993,'Hyena',96),
(994,'Kappa',96),
(995,'Lybra',96),
(996,'Monte Carlo',96),
(997,'Musa',96),
(998,'Phedra',96),
(999,'Prisma',96),
(1000,'Thema',96),
(1001,'Thesis',96),
(1002,'Trevi',96),
(1003,'Y10',96),
(1004,'Ypsilon',96),
(1005,'Zeta',96),
(1006,'Defender',97),
(1007,'Discovery',97),
(1008,'Freelander',97),
(1009,'Range Rover Evoque',97),
(1010,'Range Rover',97),
(1011,'Range Rover Sport',97),
(1012,'Series I',97),
(1013,'Series II',97),
(1014,'Series III',97),
(1015,'Fashion (CV9)',98),
(1016,'Forward',98),
(1017,'X5',98),
(1018,'X6',98),
(1019,'CT',99),
(1020,'ES',99),
(1021,'GS',99),
(1022,'GX',99),
(1023,'HS',99),
(1024,'IS',99),
(1025,'LFA',99),
(1026,'LS',99),
(1027,'LX',99),
(1028,'RC',99),
(1029,'RX',99),
(1030,'SC',99),
(1031,'Leopard',100),
(1032,'Breez (520)',101),
(1033,'Cebrium (720)',101),
(1035,'Solano (620)',101),
(1036,'X60',101),
(1037,'Aviator',102),
(1038,'Continental',102),
(1039,'LS',102),
(1040,'Mark LT',102),
(1041,'Mark VII',102),
(1042,'Mark VIII',102),
(1043,'MKC',102),
(1044,'MKS',102),
(1045,'MKT',102),
(1046,'MKX',102),
(1047,'MKZ',102),
(1048,'Navigator',102),
(1049,'Town Car',102),
(1050,'340R',103),
(1051,'Eclat',103),
(1052,'Elan',103),
(1053,'Elise',103),
(1054,'Elite',103),
(1055,'Esprit',103),
(1056,'Europa',103),
(1057,'Europa S',103),
(1058,'Evora',103),
(1059,'Excel',103),
(1060,'Exige',103),
(1061,'TX',104),
(1062,'Luxgen5',105),
(1063,'Luxgen7 MPV',105),
(1064,'Luxgen7 SUV',105),
(1065,'U6 Turbo',105),
(1066,'U7 Turbo',105),
(1067,'Armada',106),
(1068,'Bolero',106),
(1069,'CJ-3',106),
(1070,'CL',106),
(1071,'Commander',106),
(1072,'Marshal',106),
(1073,'MM',106),
(1074,'NC 640 DP',106),
(1075,'Scorpio',106),
(1076,'Verito',106),
(1077,'Voyager',106),
(1078,'Xylo',106),
(1079,'GTS',107),
(1080,'LM 400',107),
(1081,'LM 500',107),
(1082,'Mantis',107),
(1083,'Marcasite',107),
(1084,'5EXi',108),
(1085,'Sportster',108),
(1086,'B1',109),
(1087,'B2',109),
(1088,'1000',110),
(1089,'800',110),
(1090,'Alto',110),
(1091,'Baleno',110),
(1092,'Esteem',110),
(1093,'Gypsy',110),
(1094,'Omni',110),
(1095,'Versa',110),
(1096,'Wagon R',110),
(1097,'Zen',110),
(1098,'228',111),
(1099,'3200 GT',111),
(1100,'420',111),
(1101,'4200 GT',111),
(1102,'Barchetta Stradale',111),
(1103,'Biturbo',111),
(1104,'Bora',111),
(1105,'Chubasco',111),
(1106,'Ghibli',111),
(1107,'GranTurismo',111),
(1108,'Indy',111),
(1109,'Karif',111),
(1110,'Khamsin',111),
(1111,'Kyalami',111),
(1112,'MC12',111),
(1113,'Merak',111),
(1114,'Mexico',111),
(1115,'Quattroporte',111),
(1116,'Royale',111),
(1117,'Shamal',111),
(1118,'57',112),
(1119,'62',112),
(1120,'1000',113),
(1121,'121',113),
(1122,'1300',113),
(1123,'2',113),
(1124,'3',113),
(1125,'323',113),
(1126,'5',113),
(1127,'6',113),
(1128,'616',113),
(1129,'626',113),
(1130,'818',113),
(1131,'929',113),
(1132,'Atenza',113),
(1133,'Axela',113),
(1134,'AZ-1',113),
(1135,'AZ-Offroad',113),
(1136,'AZ-Wagon',113),
(1137,'B-series',113),
(1138,'Biante',113),
(1139,'Bongo Friendee',113),
(1140,'Bongo',113),
(1141,'BT-50',113),
(1142,'Capella',113),
(1143,'Carol',113),
(1144,'Cronos',113),
(1145,'CX-5',113),
(1146,'CX-7',113),
(1147,'CX-9',113),
(1148,'Demio',113),
(1149,'Efini MS-6',113),
(1150,'Efini MS-8',113),
(1151,'Efini MS-9',113),
(1152,'Eunos 300',113),
(1153,'Eunos 500',113),
(1154,'Eunos 800',113),
(1155,'Eunos Cosmo',113),
(1156,'Familia',113),
(1157,'Lantis',113),
(1158,'Laputa',113),
(1159,'Luce',113),
(1160,'Millenia',113),
(1161,'MPV',113),
(1162,'MX-3',113),
(1163,'MX-5',113),
(1164,'MX-6',113),
(1165,'Navajo',113),
(1166,'Persona',113),
(1167,'Premacy',113),
(1168,'Proceed Levante',113),
(1169,'Proceed Marvie',113),
(1170,'Protege',113),
(1171,'Revue',113),
(1172,'Roadster',113),
(1173,'RX-7',113),
(1174,'RX-8',113),
(1175,'Scrum',113),
(1176,'Sentia',113),
(1177,'Spiano',113),
(1178,'Tribute',113),
(1179,'Verisa',113),
(1180,'F1',114),
(1181,'MP4-12C',114),
(1182,'P1',114),
(1183,'Club',115),
(1184,'Monte Carlo',115),
(1185,'Track',115),
(1186,'190 (W201)',116),
(1187,'A-klasse',116),
(1188,'B-klasse',116),
(1189,'C-klasse',116),
(1190,'Citan',116),
(1191,'CL-klasse',116),
(1192,'CLA-klasse',116),
(1193,'CLC-klasse',116),
(1194,'CLK-klasse',116),
(1195,'CLS-klasse',116),
(1196,'E-klasse',116),
(1197,'G-klasse',116),
(1198,'GL-klasse',116),
(1199,'GLA-klasse',116),
(1200,'GLK-klasse',116),
(1201,'M-klasse',116),
(1202,'R-klasse',116),
(1203,'S-klasse',116),
(1204,'SL-klasse',116),
(1205,'SLK-klasse',116),
(1206,'SLR McLaren',116),
(1207,'SLS AMG',116),
(1208,'V-classe',116),
(1209,'Vaneo',116),
(1210,'Viano',116),
(1211,'W114',116),
(1212,'W115',116),
(1213,'W123',116),
(1214,'W124',116),
(1215,'Capri',117),
(1216,'Cougar',117),
(1217,'Grand Marquis',117),
(1218,'Marauder',117),
(1219,'Mariner',117),
(1220,'Marquis',117),
(1221,'Milan',117),
(1222,'Montego',117),
(1223,'Monterey',117),
(1224,'Mountaineer',117),
(1225,'Mystique',117),
(1226,'Sable',117),
(1227,'Topaz',117),
(1228,'Tracer',117),
(1229,'Villager',117),
(1230,'Metrocab I',118),
(1231,'Metrocab II (TTT)',118),
(1232,'3',119),
(1233,'350',119),
(1234,'5',119),
(1235,'550',119),
(1236,'6',119),
(1237,'F',119),
(1238,'Maestro',119),
(1239,'Metro',119),
(1240,'MGB',119),
(1241,'Midget',119),
(1242,'Montego',119),
(1243,'RV8',119),
(1244,'TF',119),
(1245,'Xpower SV',119),
(1246,'ZR',119),
(1247,'ZS',119),
(1248,'ZT',119),
(1249,'F8C',120),
(1250,'M.Go',120),
(1251,'M8',120),
(1252,'MC',120),
(1253,'Virgo',120),
(1254,'TF 1800',121),
(1255,'Cabrio',122),
(1256,'Clubman',122),
(1257,'Clubvan',122),
(1258,'Countryman',122),
(1259,'Coupe',122),
(1260,'Hatch',122),
(1261,'Paceman',122),
(1262,'Roadster',122),
(1263,'3000 GT',123),
(1264,'Airtrek',123),
(1265,'Aspire',123),
(1266,'ASX',123),
(1267,'Carisma',123),
(1268,'Celeste',123),
(1269,'Challenger',123),
(1270,'Chariot',123),
(1271,'Colt',123),
(1272,'Cordia',123),
(1273,'Debonair',123),
(1274,'Delica',123),
(1275,'Diamante',123),
(1276,'Dingo',123),
(1277,'Dion',123),
(1278,'Eclipse',123),
(1279,'eK',123),
(1280,'Emeraude',123),
(1281,'Endeavor',123),
(1282,'Eterna',123),
(1283,'FTO',123),
(1284,'Galant',123),
(1285,'Grandis',123),
(1286,'GTO',123),
(1287,'i',123),
(1288,'i-MiEV',123),
(1289,'Jeep',123),
(1290,'L200',123),
(1291,'Lancer Cargo',123),
(1292,'Lancer Evolution',123),
(1293,'Lancer',123),
(1294,'Legnum',123),
(1295,'Libero',123),
(1296,'Minica',123),
(1297,'Mirage',123),
(1298,'Montero',123),
(1299,'Montero Sport',123),
(1300,'Outlander',123),
(1301,'Pajero',123),
(1302,'Pajero iO',123),
(1303,'Pajero Junior',123),
(1304,'Pajero Mini',123),
(1305,'Pajero Pinin',123),
(1306,'Pajero Sport',123),
(1307,'Pistachio',123),
(1308,'Proudia',123),
(1309,'Raider',123),
(1310,'RVR',123),
(1311,'Sapporo',123),
(1312,'Sigma',123),
(1313,'Space Gear',123),
(1314,'Space Runner',123),
(1315,'Space Star',123),
(1316,'Space Wagon',123),
(1317,'Starion',123),
(1318,'Toppo',123),
(1319,'Town Box',123),
(1320,'Tredia',123),
(1321,'Galue 204',124),
(1322,'Galue',124),
(1323,'Himiko',124),
(1324,'Le-Seyde',124),
(1325,'Like',124),
(1326,'Micro Car K-2',124),
(1327,'Micro Car MC-1',124),
(1328,'Nouera',124),
(1329,'Orochi',124),
(1330,'Ray',124),
(1331,'Ryoga',124),
(1332,'Viewt',124),
(1333,'Yuga',124),
(1334,'Zero 1',124),
(1335,'3 Wheeler',125),
(1336,'4 Seater',125),
(1337,'4/4',125),
(1338,'Aero 8',125),
(1339,'Aero Coupe',125),
(1340,'Aero SuperSports',125),
(1341,'AeroMax',125),
(1342,'Plus 4',125),
(1343,'Plus 8',125),
(1344,'Roadster',125),
(1345,'Marina',126),
(1346,'100NX',127),
(1347,'180SX',127),
(1348,'200SX',127),
(1349,'240SX',127),
(1350,'280ZX',127),
(1351,'300ZX',127),
(1352,'350Z',127),
(1353,'370Z',127),
(1354,'AD',127),
(1355,'Almera Classic',127),
(1356,'Almera',127),
(1357,'Almera Tino',127),
(1358,'Altima',127),
(1359,'Armada',127),
(1360,'Avenir',127),
(1361,'Bassara',127),
(1362,'BE-1',127),
(1363,'Bluebird Sylphy',127),
(1364,'Bluebird',127),
(1365,'Cedric',127),
(1366,'Cefiro',127),
(1367,'Cherry',127),
(1368,'Cima',127),
(1369,'Clipper',127),
(1370,'Crew',127),
(1371,'Cube',127),
(1372,'Datsun',127),
(1373,'Dualis',127),
(1374,'Elgrand',127),
(1375,'Expert',127),
(1376,'Fairlady Z',127),
(1377,'Figaro',127),
(1378,'Fuga',127),
(1379,'Gloria',127),
(1380,'GT-R',127),
(1381,'Juke',127),
(1382,'Lafesta',127),
(1383,'Langley',127),
(1384,'Largo',127),
(1385,'Laurel',127),
(1386,'Leaf',127),
(1387,'Leopard',127),
(1388,'Liberty',127),
(1389,'Lucino',127),
(1390,'March',127),
(1391,'Maxima',127),
(1392,'Micra',127),
(1393,'Mistral',127),
(1394,'Moco',127),
(1395,'Murano',127),
(1396,'Navara (Frontier)',127),
(1397,'Note',127),
(1398,'NP 300',127),
(1399,'NV200',127),
(1400,'NX Coupe',127),
(1401,'Otti (Dayz)',127),
(1402,'Pao',127),
(1403,'Pathfinder',127),
(1404,'Patrol',127),
(1405,'Pino',127),
(1406,'M12 GTO',128),
(1407,'M600',128),
(1408,'Achieva',129),
(1409,'Alero',129),
(1410,'Aurora',129),
(1411,'Bravada',129),
(1412,'Cutlass',129),
(1413,'Cutlass Calais',129),
(1414,'Cutlass Ciera',129),
(1415,'Cutlass Supreme',129),
(1416,'Eighty-Eight',129),
(1417,'Intrigue',129),
(1418,'Ninety-Eight',129),
(1419,'Omega',129),
(1420,'Silhouette',129),
(1421,'Adam',130),
(1422,'Admiral',130),
(1423,'Agila',130),
(1424,'Ampera',130),
(1425,'Antara',130),
(1426,'Ascona',130),
(1427,'Astra',130),
(1428,'Calibra',130),
(1429,'Campo',130),
(1430,'Cascada',130),
(1431,'Combo',130),
(1432,'Commodore',130),
(1433,'Corsa',130),
(1434,'Diplomat',130),
(1435,'Frontera',130),
(1436,'GT',130),
(1437,'Insignia',130),
(1438,'Kadett',130),
(1439,'Manta',130),
(1440,'Meriva',130),
(1441,'Mokka',130),
(1442,'Monterey',130),
(1443,'Monza',130),
(1444,'Omega',130),
(1445,'Rekord',130),
(1446,'Senator',130),
(1447,'Signum',130),
(1448,'Sintra',130),
(1449,'Speedster',130),
(1450,'Tigra',130),
(1451,'Vectra',130),
(1452,'Vita',130),
(1453,'Zafira',130),
(1454,'2500 GT',131),
(1455,'Huayra',132),
(1456,'Zonda',132),
(1457,'Esperante',133),
(1458,'Roadster',133),
(1459,'Alza',134),
(1460,'Kancil',134),
(1461,'Kelisa',134),
(1462,'Kembara',134),
(1463,'Kenari',134),
(1464,'MyVi',134),
(1465,'Nautica',134),
(1466,'Viva',134),
(1467,'1007',135),
(1468,'104',135),
(1469,'106',135),
(1470,'107',135),
(1471,'2008',135),
(1472,'204',135),
(1473,'205',135),
(1474,'206',135),
(1475,'207',135),
(1476,'208',135),
(1477,'3008',135),
(1478,'301',135),
(1479,'304',135),
(1480,'305',135),
(1481,'306',135),
(1482,'307',135),
(1483,'308',135),
(1484,'309',135),
(1485,'4007',135),
(1486,'4008',135),
(1487,'405',135),
(1488,'406',135),
(1489,'407',135),
(1490,'408',135),
(1491,'5008',135),
(1492,'504',135),
(1493,'505',135),
(1494,'508',135),
(1495,'604',135),
(1496,'605',135),
(1497,'607',135),
(1498,'806',135),
(1499,'807',135),
(1501,'Partner',135),
(1502,'RCZ',135),
(1503,'Porter',136),
(1504,'Acclaim',137),
(1505,'Breeze',137),
(1506,'Caravelle',137),
(1507,'Laser',137),
(1508,'Neon',137),
(1509,'Prowler',137),
(1510,'Sundance',137),
(1511,'Turismo',137),
(1512,'Voyager',137),
(1513,'6000',138),
(1514,'Aztec',138),
(1515,'Bonneville',138),
(1516,'Fiero',138),
(1517,'Firebird',138),
(1518,'G4',138),
(1519,'G5',138),
(1520,'G6',138),
(1521,'G8',138),
(1522,'Grand AM',138),
(1523,'Grand Prix',138),
(1524,'GTO',138),
(1525,'LeMans',138),
(1526,'Montana',138),
(1527,'Parisienne',138),
(1528,'Phoenix',138),
(1529,'Solstice',138),
(1530,'Sunbird',138),
(1531,'Sunfire',138),
(1532,'Tempest',138),
(1533,'Torrent',138),
(1534,'Trans Sport',138),
(1535,'Vibe',138),
(1536,'911',139),
(1537,'914',139),
(1538,'918',139),
(1539,'924',139),
(1540,'928',139),
(1541,'944',139),
(1542,'959',139),
(1543,'968',139),
(1544,'Boxster',139),
(1545,'Carrera GT',139),
(1546,'Cayenne',139),
(1547,'Cayman',139),
(1548,'Macan',139),
(1549,'Panamera',139),
(1550,'118NE',140),
(1551,'Padmini',140),
(1552,'Exora',141),
(1553,'Gen-2',141),
(1554,'Inspira',141),
(1555,'Juara',141),
(1556,'Perdana',141),
(1557,'Persona',141),
(1558,'Preve',141),
(1559,'Saga',141),
(1560,'Satria',141),
(1561,'Waja',141),
(1562,'Wira (400 Series)',141),
(1563,'G-modell',142),
(1564,'Pinzgauer',142),
(1565,'GTB',143),
(1566,'GTE',143),
(1567,'3',144),
(1568,'Mangusta',145),
(1569,'Scimitar Sabre',146),
(1570,'11',147),
(1571,'12',147),
(1572,'14',147),
(1573,'15',147),
(1574,'16',147),
(1575,'17',147),
(1576,'18',147),
(1577,'19',147),
(1578,'20',147),
(1579,'21',147),
(1580,'25',147),
(1581,'30',147),
(1582,'4',147),
(1583,'5',147),
(1584,'6',147),
(1585,'9',147),
(1586,'Avantime',147),
(1587,'Captur',147),
(1588,'Clio',147),
(1589,'Duster',147),
(1590,'Espace',147),
(1591,'Estafette',147),
(1592,'Fluence',147),
(1593,'Fuego',147),
(1594,'Kangoo',147),
(1595,'Koleos',147),
(1596,'Laguna',147),
(1597,'Latitude',147),
(1598,'Logan',147),
(1599,'Megane',147),
(1600,'Modus',147),
(1601,'Rodeo',147),
(1602,'Safrane',147),
(1603,'Sandero',147),
(1604,'Scenic',147),
(1605,'Sport Spider',147),
(1606,'Symbol',147),
(1607,'Twingo',147),
(1608,'Twizy',147),
(1609,'Vel Satis',147),
(1610,'Wind',147),
(1611,'ZOE',147),
(1612,'SM3',148),
(1613,'SM5',148),
(1614,'SM7',148),
(1615,'Corniche',149),
(1616,'Ghost',149),
(1617,'Park Ward',149),
(1618,'Phantom',149),
(1619,'Silver Seraph',149),
(1620,'Silver Spur',149),
(1621,'Wraith',149),
(1622,'Lightning',150),
(1623,'100',151),
(1624,'200',151),
(1625,'25',151),
(1626,'400',151),
(1627,'45',151),
(1628,'600',151),
(1629,'75',151),
(1630,'800',151),
(1631,'Metro',151),
(1632,'Mini',151),
(1633,'P6',151),
(1634,'SD1',151),
(1635,'9-2X',152),
(1636,'9-3',152),
(1637,'9-4X',152),
(1638,'9-5',152),
(1639,'9-7X',152),
(1640,'90',152),
(1641,'900',152),
(1642,'9000',152),
(1643,'95',152),
(1644,'96',152),
(1645,'99',152),
(1646,'S7',153),
(1647,'PS-10',154),
(1648,'Astra',155),
(1649,'Aura',155),
(1650,'ION',155),
(1651,'LS',155),
(1652,'LW',155),
(1653,'Outlook',155),
(1654,'Relay',155),
(1655,'SC',155),
(1656,'Sky',155),
(1657,'SL',155),
(1658,'SW',155),
(1659,'VUE',155),
(1660,'FR-S',156),
(1661,'iQ',156),
(1662,'tC',156),
(1663,'xA',156),
(1664,'xB',156),
(1665,'xD',156),
(1666,'133',157),
(1667,'Alhambra',157),
(1668,'Altea',157),
(1669,'Arosa',157),
(1670,'Cordoba',157),
(1671,'Exeo',157),
(1672,'Fura',157),
(1673,'Ibiza',157),
(1674,'Leon',157),
(1675,'Malaga',157),
(1676,'Marbella',157),
(1677,'Ronda',157),
(1678,'Toledo',157),
(1679,'Noble',158),
(1680,'Sceo',158),
(1681,'100 Series',159),
(1682,'Citigo',159),
(1683,'Fabia',159),
(1684,'Favorit',159),
(1685,'Felicia',159),
(1686,'Octavia',159),
(1687,'Rapid',159),
(1688,'Roomster',159),
(1689,'Superb',159),
(1690,'Yeti',159),
(1691,'Forfour',160),
(1692,'Fortwo',160),
(1693,'Roadster',160),
(1694,'Lioncel',161),
(1695,'R42',162),
(1696,'C12',163),
(1697,'C8',163),
(1699,'Actyon',165),
(1700,'Actyon Sports',165),
(1701,'Chairman',165),
(1702,'Kallista',165),
(1703,'Korando Family',165),
(1704,'Korando',165),
(1705,'Kyron',165),
(1706,'Musso',165),
(1707,'Rexton',165),
(1708,'Rodius',165),
(1709,'Stavic',165),
(1710,'Alcyone',166),
(1711,'Baja',166),
(1712,'BRZ',166),
(1713,'Dex',166),
(1714,'Domingo',166),
(1715,'Exiga',166),
(1716,'Forester',166),
(1717,'Impreza',166),
(1718,'Justy',166),
(1719,'Legacy',166),
(1720,'Leone',166),
(1721,'Libero',166),
(1722,'Lucra',166),
(1723,'Outback',166),
(1724,'Pleo',166),
(1725,'R1',166),
(1726,'R2',166),
(1727,'Sambar',166),
(1728,'Stella',166),
(1729,'SVX',166),
(1730,'Traviq',166),
(1731,'Trezia',166),
(1732,'Tribeca',166),
(1733,'Vivio',166),
(1734,'XT',166),
(1735,'XV',166),
(1736,'Aerio',167),
(1737,'Alto',167),
(1738,'Baleno',167),
(1739,'Cappuccino',167),
(1740,'Cervo',167),
(1741,'Ertiga',167),
(1742,'Escudo',167),
(1743,'Every',167),
(1744,'Forenza',167),
(1745,'Grand Vitara',167),
(1746,'Ignis',167),
(1747,'Jimny',167),
(1748,'Kei',167),
(1749,'Kizashi',167),
(1750,'Landy',167),
(1751,'Liana',167),
(1752,'MR Wagon',167),
(1753,'Palette',167),
(1754,'Reno',167),
(1755,'Solio',167),
(1756,'Spacia',167),
(1757,'Splash',167),
(1758,'Swift',167),
(1759,'SX4',167),
(1760,'Verona',167),
(1761,'Wagon R',167),
(1762,'Wagon R+',167),
(1763,'X-90',167),
(1764,'XL7',167),
(1765,'1510',168),
(1766,'Avenger',168),
(1767,'Horizon',168),
(1768,'Samba',168),
(1769,'Solara',168),
(1770,'Tagora',168),
(1771,'Aria',169),
(1772,'Estate',169),
(1773,'Indica',169),
(1774,'Indigo',169),
(1775,'Nano',169),
(1776,'Safari',169),
(1777,'Sierra',169),
(1778,'Sumo',169),
(1779,'T613',170),
(1780,'T700',170),
(1781,'Zero',171),
(1782,'Model S',172),
(1783,'Roadster',172),
(1784,'Century',173),
(1785,'Admiral',174),
(1786,'Dogan',175),
(1787,'Kartal',175),
(1788,'Murat 131',175),
(1789,'Sahin',175),
(1790,'Serce',175),
(1791,'4runner',176),
(1792,'Allex',176),
(1793,'Allion',176),
(1794,'Alphard',176),
(1795,'Altezza',176),
(1796,'Aristo',176),
(1797,'Aurion',176),
(1798,'Auris',176),
(1799,'Avalon',176),
(1800,'Avensis',176),
(1801,'Avensis Verso',176),
(1802,'Aygo',176),
(1803,'bB',176),
(1804,'Belta',176),
(1805,'Blade',176),
(1806,'Blizzard',176),
(1807,'Brevis',176),
(1808,'Caldina',176),
(1809,'Cami',176),
(1810,'Camry',176),
(1811,'Carina ED',176),
(1812,'Carina',176),
(1813,'Cavalier',176),
(1814,'Celica',176),
(1815,'Celsior',176),
(1816,'Century',176),
(1817,'Chaser',176),
(1818,'Corolla',176),
(1819,'Corolla Rumion',176),
(1820,'Corolla Verso (Spacio)',176),
(1821,'Corona',176),
(1822,'Corsa',176),
(1823,'Cressida',176),
(1824,'Cresta',176),
(1825,'Crown Majesta',176),
(1826,'Crown',176),
(1827,'Curren',176),
(1828,'Cynos',176),
(1829,'Duet',176),
(1830,'Echo',176),
(1831,'Estima',176),
(1832,'FJ Cruiser',176),
(1833,'Fortuner',176),
(1834,'FunCargo',176),
(1835,'Gaia',176),
(1836,'Granvia',176),
(1837,'GT 86',176),
(1838,'Harrier',176),
(1839,'HiAce',176),
(1840,'Highlander',176),
(1841,'Hilux Surf',176),
(1842,'Hilux',176),
(1843,'Innova',176),
(1844,'Ipsum',176),
(1845,'iQ',176),
(1846,'ISis',176),
(1847,'Ist',176),
(1848,'Kluger',176),
(1849,'Land Cruiser',176),
(1850,'Land Cruiser Prado',176),
(1851,'1.1',177),
(1852,'P 601',177),
(1853,'Tramontana',178),
(1854,'TR7',179),
(1855,'TR8',179),
(1856,'280',180),
(1857,'350',180),
(1858,'390',180),
(1859,'400',180),
(1860,'420',180),
(1861,'450',180),
(1862,'Cerbera',180),
(1863,'Chimaera',180),
(1864,'Griffith',180),
(1865,'S-Series',180),
(1866,'Sagaris',180),
(1867,'Tamora',180),
(1868,'Tuscan',180),
(1869,'Adam',181),
(1870,'Agila',181),
(1871,'Astra',181),
(1872,'Calibra',181),
(1873,'Carlton',181),
(1874,'Cavalier',181),
(1875,'Chevette',181),
(1876,'Combo',181),
(1877,'Corsa',181),
(1878,'Frontera',181),
(1879,'Monterey',181),
(1880,'Nova',181),
(1881,'Omega',181),
(1882,'Royale',181),
(1883,'Senator',181),
(1884,'Sintra',181),
(1885,'Tigra',181),
(1886,'Vectra',181),
(1887,'Ventora',181),
(1888,'Viceroy',181),
(1889,'Victor',181),
(1890,'Viva',181),
(1891,'VXR8',181),
(1892,'Zafira',181),
(1893,'M12',182),
(1894,'W8 Twin Turbo',182),
(1895,'210',183),
(1896,'260 LM',183),
(1897,'300 Atlantique',183),
(1898,'400 GT',183),
(1899,'181',184),
(1900,'Beetle',184),
(1901,'Bora',184),
(1902,'Caddy',184),
(1903,'Corrado',184),
(1904,'Derby',184),
(1905,'Eos',184),
(1906,'Fox',184),
(1907,'Golf Country',184),
(1908,'Golf',184),
(1909,'Golf Plus',184),
(1910,'Golf Sportsvan',184),
(1911,'Iltis',184),
(1912,'Jetta',184),
(1913,'Kaefer',184),
(1914,'Lupo',184),
(1915,'Multivan',184),
(1916,'Passat (North America)',184),
(1917,'Passat',184),
(1918,'Passat CC',184),
(1919,'Phaeton',184),
(1920,'Pointer',184),
(1921,'Polo',184),
(1922,'Routan',184),
(1923,'Santana',184),
(1924,'Scirocco',184),
(1925,'Sharan',184),
(1926,'Taro',184),
(1927,'Tiguan',184),
(1928,'Touareg',184),
(1929,'Touran',184),
(1930,'Type 4',184),
(1931,'up!',184),
(1932,'Vento',184),
(1933,'XL1',184),
(1934,'140 Series',185),
(1935,'164',185),
(1936,'240 Series',185),
(1937,'260 Series',185),
(1938,'300 Series',185),
(1939,'440',185),
(1940,'460',185),
(1941,'480',185),
(1942,'66',185),
(1943,'740',185),
(1944,'760',185),
(1945,'780',185),
(1946,'850',185),
(1947,'940',185),
(1948,'960',185),
(1949,'C30',185),
(1950,'C70',185),
(1951,'Laplander',185),
(1952,'S40',185),
(1953,'S60',185),
(1954,'S70',185),
(1955,'S80',185),
(1956,'S90',185),
(1957,'V40',185),
(1958,'V50',185),
(1959,'V60',185),
(1960,'V70',185),
(1961,'V90',185),
(1962,'XC60',185),
(1963,'XC70',185),
(1964,'XC90',185),
(1965,'Corda',186),
(1966,'Estina',186),
(1967,'Tingo',186),
(1968,'1.3',187),
(1969,'353',187),
(1970,'SEi & Sport',188),
(1971,'SEiGHT',188),
(1972,'GT',189),
(1973,'Roadster',189),
(1974,'Pickup X3',190),
(1975,'SR-V X3',190),
(1976,'SUV X3',190),
(1977,'10',191),
(1978,'Florida',191),
(1979,'Skala',191),
(1980,'Yugo',191),
(1981,'Nomad (RX6400)',192),
(1982,'Admiral',193),
(1983,'GrandTiger',193),
(1984,'Landmark',193),
(1985,'Ё-Кроссовер (Опытный образец)',211),
(1986,'2160',212),
(1987,'2163',212),
(1988,'3101',212),
(1989,'Ока-Астро 11301',213),
(1990,'Рысь',214),
(1991,'1111 Ока',215),
(1992,'2101',215),
(1993,'2102',215),
(1994,'2103',215),
(1995,'2104',215),
(1996,'2105',215),
(1997,'2106',215),
(1998,'2107',215),
(1999,'2108',215),
(2000,'2109',215),
(2001,'21099',215),
(2002,'2110',215),
(2003,'2111',215),
(2004,'2112',215),
(2005,'2113',215),
(2006,'2114',215),
(2007,'2115',215),
(2008,'2120 Надежда',215),
(2009,'2121 (4x4)',215),
(2010,'2123',215),
(2011,'2129',215),
(2012,'2131 (4x4)',215),
(2013,'2328',215),
(2014,'2329',215),
(2015,'Granta',215),
(2016,'Kalina',215),
(2017,'Largus',215),
(2018,'Priora',215),
(2019,'Revolution',215),
(2020,'13 «Чайка»',216),
(2021,'14 «Чайка»',216),
(2022,'21 «Волга»',216),
(2023,'22 «Волга»',216),
(2024,'2308 «Атаман»',216),
(2025,'2330 «Тигр»',216),
(2026,'24 «Волга»',216),
(2027,'3102 «Волга»',216),
(2028,'31029 «Волга»',216),
(2029,'3103 «Волга»',216),
(2030,'3105 «Волга»',216),
(2031,'3110 «Волга»',216),
(2032,'31105 «Волга»',216),
(2033,'3111 «Волга»',216),
(2034,'69',216),
(2035,'Volga Siber',216),
(2036,'М-20 «Победа»',216),
(2037,'М1',216),
(2038,'1102 «Таврия»',217),
(2039,'1103 «Славута»',217),
(2040,'1105 «Дана»',217),
(2041,'965',217),
(2042,'966',217),
(2043,'968',217),
(2044,'Chance',217),
(2045,'Forza',217),
(2046,'Sens',217),
(2047,'Vida',217),
(2048,'114',218),
(2049,'117',218),
(2050,'4104',218),
(2051,'2125 «Комби»',219),
(2052,'2126 «Ода»',219),
(2053,'21261 «Фабула»',219),
(2054,'2717',219),
(2055,'Москвич-412',219),
(2056,'1111 Ока',220),
(2057,'2317',221),
(2058,'1302 Волынь',222),
(2059,'967',222),
(2060,'969',222),
(2061,'2136',223),
(2062,'2137',223),
(2063,'2138',223),
(2064,'2140',223),
(2065,'2141',223),
(2066,'400',223),
(2067,'401',223),
(2068,'402',223),
(2069,'403',223),
(2070,'407',223),
(2071,'408',223),
(2072,'410',223),
(2073,'411',223),
(2074,'412',223),
(2075,'423',223),
(2076,'426',223),
(2077,'427',223),
(2078,'Дуэт',223),
(2079,'Иван Калита',223),
(2080,'Князь Владимир',223),
(2081,'Святогор',223),
(2082,'Юрий Долгорукий',223),
(2083,'С-3А',224),
(2084,'С-3Д',224),
(2085,'1111 Ока',225),
(2086,'Aquila',226),
(2087,'C-30',226),
(2088,'C10',226),
(2089,'C190',226),
(2090,'Road Partner',226),
(2091,'Tager',226),
(2092,'Vega',226),
(2093,'3151',227),
(2094,'3153',227),
(2095,'3159 Барс',227),
(2096,'3160',227),
(2097,'3162 Simbir',227),
(2098,'469',227),
(2099,'Hunter',227),
(2100,'Patriot',227),
(2101,'Pickup',227),
(2102,'Traverse',35),
(2103,'Uplander',35),
(2104,'Van',35),
(2105,'Vectra',35),
(2106,'Venture',35),
(2107,'Viva',35),
(2108,'Volt',35),
(2109,'Zafira',35),
(2110,'S30',52),
(2111,'GC6',67),
(2112,'Q70',80),
(2113,'Celliya (530)',101),
(2114,'Xedos 6',113),
(2115,'Xedos 9',113),
(2116,'Pixo',127),
(2117,'Prairie',127),
(2118,'Presage',127),
(2119,'Presea',127),
(2120,'President',127),
(2121,'Primera',127),
(2122,'Pulsar',127),
(2123,'Qashqai',127),
(2124,'Quest',127),
(2125,'Rnessa',127),
(2126,'Rasheen',127),
(2127,'Rogue',127),
(2128,'Roox',127),
(2129,'Safari',127),
(2130,'Sentra',127),
(2131,'Serena',127),
(2132,'Silvia',127),
(2133,'Skyline Crossover',127),
(2134,'Skyline',127),
(2135,'Stagea',127),
(2136,'Stanza',127),
(2137,'Sunny',127),
(2138,'Teana',127),
(2139,'Terrano',127),
(2140,'Terrano Regulus',127),
(2141,'Tiida',127),
(2142,'Tino',127),
(2143,'Titan',127),
(2144,'Vanette',127),
(2145,'Versa',127),
(2146,'Wingroad',127),
(2147,'X-Terra',127),
(2148,'X-Trail',127),
(2149,'LiteAce',176),
(2150,'Mark II',176),
(2151,'Mark X',176),
(2152,'Mark X ZiO',176),
(2153,'MasterAce Surf',176),
(2154,'Matrix',176),
(2155,'Mega Cruiser',176),
(2156,'MR2',176),
(2157,'Nadia',176),
(2158,'Noah',176),
(2159,'Opa',176),
(2160,'Origin',176),
(2161,'Paseo',176),
(2162,'Passo',176),
(2163,'Passo Sette',176),
(2164,'Picnic',176),
(2165,'Platz',176),
(2166,'Porte',176),
(2167,'Premio',176),
(2168,'Previa',176),
(2169,'Prius c',176),
(2170,'Prius',176),
(2171,'Prius v',176),
(2172,'Probox',176),
(2173,'Progres',176),
(2174,'Pronard',176),
(2175,'Ractis',176),
(2176,'Raum',176),
(2177,'RAV 4',176),
(2178,'Regius',176),
(2179,'RegiusAce',176),
(2180,'Rush',176),
(2181,'Sai',176),
(2182,'Scepter',176),
(2183,'Sequoia',176),
(2184,'Sera',176),
(2185,'Sienna',176),
(2186,'Sienta',176),
(2187,'Soarer',176),
(2188,'Soluna',176),
(2189,'Sparky',176),
(2190,'Sprinter Carib',176),
(2191,'Sprinter Marino',176),
(2192,'Sprinter Trueno',176),
(2193,'Sprinter',176),
(2194,'Starlet',176),
(2195,'Succeed',176),
(2196,'Supra',176),
(2197,'Tacoma',176),
(2198,'Tercel',176),
(2199,'TownAce',176),
(2200,'Tundra',176),
(2201,'Urban Cruiser',176),
(2202,'Vanguard',176),
(2203,'Vellfire',176),
(2204,'Venza',176),
(2205,'Verossa',176),
(2206,'Verso',176),
(2207,'Verso-S',176),
(2208,'Vios',176),
(2209,'Vista',176),
(2210,'Vitz',176),
(2211,'Voltz',176),
(2212,'Voxy',176),
(2213,'WiLL Cypha',176),
(2214,'WiLL',176),
(2215,'Windom',176),
(2216,'Wish',176),
(2217,'Yaris',176),
(2218,'Yaris Verso',176),
(2219,'ELR',28),
(2220,'Silverado',35),
(2221,'Jumpy',37),
(2222,'Scudo',61),
(2223,'Bronco II',63),
(2224,'Vezel',76),
(2225,'Vito',116),
(2226,'Primastar',127),
(2227,'Vivaro',130),
(2228,'108',135),
(2229,'Expert',135),
(2230,'Trafic',147),
(2231,'WRX',166),
(2232,'Celerio',167),
(2233,'Amarok',184),
(2234,'California',184),
(2235,'Caravelle',184),
(2236,'Transporter',184),
(2237,'F5',26),
(2238,'Urvan',47),
(2239,'Tourneo Custom',63),
(2240,'Grace',79),
(2241,'650S',114),
(2242,'Minicab',123),
(2243,'Caravan',127),
(2244,'NV350 Caravan',127),
(2245,'Urvan',127),
(2246,'Bipper',135),
(2247,'CM-8',32),
(2248,'M11 (A3)',34),
(2249,'Q40',80),
(2250,'X9',98),
(2251,'Vitara',167),
(2252,'Encore',25),
(2253,'Bonus 3 (E3)',34),
(2254,'Tiggo 5',34),
(2255,'SS',35),
(2256,'Lancer',51),
(2257,'Besturn X80',59),
(2258,'SC7',67),
(2259,'Sesto Elemento',95),
(2260,'NX',99),
(2261,'GTR',280),
(2265,'TLX',2),
(2266,'Disco Volante',3),
(2267,'Arizo 7',34),
(2268,'mi-DO',47),
(2269,'Renegade',89),
(2270,'Smily',101),
(2271,'Boliger',282),
(2272,'Tropica Roadster',284);

UNLOCK TABLES;

/*Table structure for table `car_service` */

CREATE TABLE `car_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  KEY `service_id` (`service_id`),
  CONSTRAINT `car_service_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `car_service_ibfk_2` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `car_service` */

LOCK TABLES `car_service` WRITE;

insert  into `car_service`(`id`,`category_id`,`service_id`) values 
(1,1,1),
(2,1,2),
(3,5,9);

UNLOCK TABLES;

/*Table structure for table `categories` */

CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(500) DEFAULT NULL,
  `slug` varchar(700) DEFAULT NULL,
  `keywords` varchar(500) DEFAULT NULL,
  `description` text,
  `img_name` varchar(300) DEFAULT NULL,
  `created_at` varchar(255) DEFAULT NULL,
  `updated_at` varchar(255) DEFAULT NULL,
  `is_status` tinyint(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `categories` */

LOCK TABLES `categories` WRITE;

insert  into `categories`(`id`,`title`,`slug`,`keywords`,`description`,`img_name`,`created_at`,`updated_at`,`is_status`) values 
(1,'Двигатель','dvigatel','','','1523546041_H7gHXr.jpg','1528464840','1528464840',1),
(2,'Кузов','kuzov','','','1523546063_EscgeX.jpg','1528464857','1528464857',1),
(3,'Подвеска','podveska','','','1523546089_ts-VeW.png','1528464873','1528464873',1),
(4,'КПП','kpp','','','1523546108_Jo6asZ.jpg','1528464895','1528464895',1),
(5,'Развал/колеса/кондиционер','razval-kolesa-kondicioner','','','1523546134_LIzWvL.jpg','1528464921','1528464921',1),
(6,'Запчасти/автострахование','zapchasti-avtostrahovanie','','','1523546157_Bfts5A.jpg','1528464956','1528464956',1);

UNLOCK TABLES;

/*Table structure for table `grid_sort` */

CREATE TABLE `grid_sort` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `visible_columns` text,
  `default_columns` text,
  `page_size` varchar(300) DEFAULT NULL,
  `class_name` varchar(300) DEFAULT NULL,
  `theme` varchar(300) DEFAULT NULL,
  `label` varchar(300) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Data for the table `grid_sort` */

LOCK TABLES `grid_sort` WRITE;

insert  into `grid_sort`(`id`,`visible_columns`,`default_columns`,`page_size`,`class_name`,`theme`,`label`,`user_id`) values 
(7,'[\"id\",\"title\",\"created_at\",\"updated_at\"]',NULL,'','common\\models\\Pages','','Pages',1),
(8,'[\"id\",\"title\",\"parent_id\",\"keywords\",\"created_at\",\"updated_at\"]',NULL,'','common\\models\\Menu','','Menus',1),
(9,'[\"id\",\"title\",\"slug\",\"keywords\",\"description\",\"updated_at\",\"created_at\"]',NULL,'','common\\models\\Categories','','Categories',1),
(10,'[\"id\",\"name\",\"seo_keyword\",\"seo_description\",\"created_at\",\"updated_at\",\"email\",\"is_status\"]',NULL,'','common\\models\\Service','','Services',1);

UNLOCK TABLES;

/*Table structure for table `menu` */

CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(500) DEFAULT NULL,
  `parent_id` int(3) DEFAULT NULL,
  `keywords` varchar(500) DEFAULT NULL,
  `slug` varchar(300) DEFAULT NULL,
  `description` text,
  `created_at` varchar(255) DEFAULT NULL,
  `updated_at` varchar(255) DEFAULT NULL,
  `is_status` tinyint(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `menu` */

LOCK TABLES `menu` WRITE;

insert  into `menu`(`id`,`title`,`parent_id`,`keywords`,`slug`,`description`,`created_at`,`updated_at`,`is_status`) values 
(1,'Для посетителей',NULL,'','pages/dlya-posetiteley','',NULL,NULL,1),
(2,'Для автосервисов',NULL,'','pages/dlya-avtoservisov','','1528318623','1528318623',1),
(3,'Правила оказания информационных услуг',NULL,'','pages/pravila-okazaniya-informacionnyh-uslug','','1528318752','1528318752',1),
(4,'Контакты',NULL,'','pages/kontakty','','1528318784','1528318784',1);

UNLOCK TABLES;

/*Table structure for table `migration` */

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `migration` */

LOCK TABLES `migration` WRITE;

insert  into `migration`(`version`,`apply_time`) values 
('m000000_000000_base',1528029396),
('m130524_201442_init',1528029400);

UNLOCK TABLES;

/*Table structure for table `pages` */

CREATE TABLE `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(300) DEFAULT NULL,
  `content` text,
  `keywords` varchar(300) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `slug` varchar(500) DEFAULT NULL,
  `created_at` varchar(300) DEFAULT NULL,
  `updated_at` varchar(300) DEFAULT NULL,
  `is_status` tinyint(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `pages` */

LOCK TABLES `pages` WRITE;

insert  into `pages`(`id`,`title`,`content`,`keywords`,`description`,`slug`,`created_at`,`updated_at`,`is_status`) values 
(2,'Для посетителей','<p>Добро пожаловать на сайт autOK.pro!<br></p><p>\r\n                    Наш сервис предназначен для создания коммуникаций между клиентами и автосервисами.\r\n                    Теперь ваш автомобиль не только в надежных руках, но вы еще и получаете скидку от\r\n                    прайсовой цены, и это только на нашем сервисе! Выберите марку и модель вашего\r\n                    автомобиля и введите какое требуется обслуживание. Звонок от мастер-приемщика поступит\r\n                    через минуту, и вы получите полный спектр профессионального и качественного\r\n                    обслуживания своего автомобиля. Все услуги сайта для клиента совершенно бесплатны. Если\r\n                    у вас возникли претензии к качеству выполненных работ, сообщите нам об этом на\r\n                    электронную почту admin@autok.pro</p>','','','dlya-posetiteley','1528047228','1528047228',1),
(3,'Для автосервисов','<p>\r\n                    Добро пожаловать на сайт autOK.pro!\r\n                </p><p>\r\n                    Наш сервис предназначен для создания коммуникаций между клиентами и автосервисами.\r\n                    Разместитесь у нас на сайте, и клиенты с легостью смогут вас находить. Все что вам нужно,\r\n                    это желание сотрудничать с нами. Мы сами сделаем красивые фотографии вашего\r\n                    автосервиса для размещения их на сайте, сделаем описание ваших работ и приложим\r\n                    максимум усилий для продвижения ваших услуг. Размещение у нас бесплатное, вы платите\r\n                    только за уведомления от ваших клиентов, полученные с сайта.\r\n                </p><p>Если вы являетесь представителем автомагазина по продаже запасных частей или представителем автострахователя, то для вас у нас предусмотрены специальные условия размещения. Свяжитесь с нами по тел: 8(950)757-91-79 и мы обязательно озвучим условия нашего сотрудничества.</p>','','','dlya-avtoservisov','1528318309','1528318309',1),
(4,'Правила оказания информационных услуг','<ol>\r\n                    <li><strong>Общие положения</strong></li>\r\n                </ol><p>1.1. Данный документ, адресованный любому лицу (далее — Клиент или Пользователь), является\r\n                    официальным предложением сайта <a href=\"http://autOK.ru/\">http://autOK.pro/</a> и содержит все\r\n                    существенные условия предоставления информационных услуг, направленных на внесение заявки\r\n                    Пользователя в программно-информационный комплекс и информирование Пользователя об исполнении\r\n                    заявки.\r\n                </p><p> 1.2. В соответствии со статьей 437 Гражданского Кодекса Российской Федерации (ГК РФ) данный документ\r\n                    является публичной Офертой. Заказ информационной услуги посредством сайта <a href=\"http://autOK.ru/\">http://autOK.pro/</a> любым из указанных в настоящей Оферте способом,\r\n                    является ее акцептом, что считается равносильным заключению договора на изложенных в ней условиях.\r\n                    Оферта размещена в том числе на официальном интернет-сайте <a href=\"http://autOK.ru/\">http://autOK.pro/</a>\r\n                </p><p>1.3. Сайт <a href=\"http://autOK.ru/\">http://autOK.pro/</a> оказывает Пользователю информационные\r\n                    услуги, направленные на внесение услуги оказываемой Пользователем в программно-информационный\r\n                    комплекс и информирование Пользователя обо всех изменеиях данной услуги. Сайт <a href=\"http://autOK.ru/\">http://autOK.ru/</a> является организацией, оказывающей Пользователю\r\n                    услуги по информированию о спросе на услуги, предоставляемые Пользователем населению, стоимость\r\n                    которых определяет сам Пользователем. Пользователь обязан, при использовании сайта<a href=\"http://autOK.ru/\">http://autOK.ru/</a> указывать стоимость своих услуг для населения.\r\n                    В случае неверного или не корректного информирования о стоимости предоставляемых услуг Пользователем\r\n                    населению, аккаунт Пользователя подлежит немедленной блокировке без возврата денежных средств\r\n                    оплаченных Пользователем за предоставление информационных услуг.\r\n                </p><p>1.4. Внимательно ознакомьтесь с текстом настоящей Оферты, и, в случае если Вы не согласны с\r\n                    какими-либо ее условиями, Вам предлагается отказаться от Услуг сайта <a href=\"http://autOK.ru/\">http://autOK.pro/</a> </p><p>1.5. Определения, используемые в целях выполнения условий настоящей Оферты:\r\n                </p><p>1.5.1. Услуги — информационные услуги, направленные на прием, обработку и передачу заказа для\r\n                    Пользователя и информирование Пользователя о наличии заказа.\r\n                </p><p>1.5.2. Заказ – обработанный системой заказ на запрашиваемую Клиентом услугу, оказываемой\r\n                    Пользователем.\r\n                </p><p>1.5.3. Пользователь — лицо, предоставляющее услуги населению в сфере авто индустрии.\r\n                </p><p>1.5.4. Клиент – лицо, которое посредством сайта <a href=\"http://autOK.ru/\">http://autOK.pro/</a>\r\n                    разместило заказ на поиск ему (либо указанным им лицам) Пользователя, который в дальнейшем будет\r\n                    оказывать услуги.\r\n                </p><ol>\r\n                    <li><strong>Предмет оферты</strong></li>\r\n                </ol><p>2.1. Сайт <a href=\"http://autOK.ru/\">http://autOK.pro/</a> оказывает информационные услуги,\r\n                    направленные на передачу заказа Клиента Пользователю и информирование Пользователя об исполнении\r\n                    заявки.\r\n                </p><p>2.2 Сайт <a href=\"http://autOK.ru/\">http://autOK.pro/</a> оказывает услуги по привлечению Клиентов на\r\n                    сайт путем различных маркетинговых мероприятий, повышая спрос на услуги Пользователей.\r\n                </p><ol>\r\n                    <li><strong>Порядок оказания услуг</strong></li>\r\n                </ol><p>3.1. Акцепт Пользователем условий настоящей Оферты осуществляется выполнением любого из следующих\r\n                    действий:\r\n                </p><p>3.1.2. Оплата услуги путем безналичного перечисления на реквизиты расчетного счета указанные в\r\n                    разделе Контакты на сайте <a href=\"http://autOK.ru/\">http://autOK.pro/</a>\r\n                </p><p> 3.1.3. Оплата услуги через бесплатное официальное мобильное приложение на базе iOS или Android,\r\n                    опубликованных для скачивания на сайте <a href=\"http://autOK.ru/\">http://autOK.pro/</a> и на\r\n                    официальных интернет-магазинах AppStore или GooglePlay соответственно.\r\n                </p><p>3.2. Пользователь несет ответственность за содержание и достоверность информации, предоставленной при\r\n                    размещении на сайте <a href=\"http://autOK.ru/\">http://autOK.pro/</a>\r\n                </p><p>3.3. После подписания данного договора данные о Пользователе регистрируются в базе данных сайта <a href=\"http://autOK.ru/\">http://autOK.pro/</a> . Сайт <a href=\"http://autOK.ru/\">http://autOK.pro/</a>\r\n                    не изменяет и не редактирует информацию Пользователя без его согласия.\r\n                </p><p>3.4. Сайт <a href=\"http://autOK.ru/\">http://autOK.pro/</a> безвозмездно размещает информацию о\r\n                    Пользователе, о предоставляемых им услугах в разделах сайта, доступных для просмотра и поиска\r\n                    Клиентом, без ограничения срока нахождения, осуществляет информирование о желании Клиента получить\r\n                    обратный звонок от мастера-приемщика или администратора, или любого уполномоченного сотрудника\r\n                    Пользователя на первичное взаимодействие с Клиентами посредством SMS сообщений, собщениями в\r\n                    программно-информационном комплексе на платной основе. Ответственность за оказание услуги перед\r\n                    Клиентом несет Пользователь.\r\n                </p><p>3.5.Сайт <a href=\"http://autOK.ru/\">http://autOK.pro/</a> оставляет за собой право отказать\r\n                    Пользователю в предоставлении услуг, выражающему несогласие с условиями настоящей Оферты, без\r\n                    объяснения причин отказа.\r\n                </p><p>3.6. Все претензии как Пользователь так и Клиент вправе направить на адрес электронной почты:\r\n                    info@autOK.pro</p><p>3.7. При сборе и обработке персональных данных Пользователя, сайт <a href=\"http://autOK.ru/\">http://autOK.pro/</a>\r\n                    руководствуется положениями Федерального закона от 27 июля 2006 года № 152 «О персональных данных»,\r\n                    а также порядком сбора и обработки персональных данных, установленным в разделе 10 настоящей Оферты.\r\n                </p><ol>\r\n                    <li><strong>Обязанности сайта </strong><a href=\"http://autOK.ru/\"><strong></strong></a><strong><a href=\"http://autOK.ru/\">http://autOK.pro/</a></strong>\r\n                    </li>\r\n                </ol><p>4.1.Разместить информацию о Пользователе в своем программно-информационном комплексе на бесплатной\r\n                    основе, проведя ряд мероприятий, для демонстрации работы Пользователя как профессионального\r\n                    представителя авто индустрии и предоставить доступ ко всем сервисам.\r\n                </p><p>4.2. Информировать Пользователя о всех изменениях происходящих на сайте <a href=\"http://autOK.ru/\">http://autOK.pro/</a>\r\n                    .\r\n                </p><p>4.3. Информировать Пользователя о случаях невозможности его размещения на сайта <a href=\"http://autOK.ru/\">http://autOK.pro/</a> при невыполнении Пользователем настоящих\r\n                    правил.\r\n                </p><p>4.4. Своевременно обнаруживать и предотвращать попытки несанкционированного доступа к информации,\r\n                    предоставляемой Пользователем.\r\n                </p><ol>\r\n                    <li><strong>Обязанности Пользователя</strong></li>\r\n                </ol><p>5.1. Сообщать о себе и о стоимости предоставляемых услуг только достоверную информацию.\r\n                </p><p>5.2. В личном кабинете оставлять изображения исключительно своих работ.\r\n                </p><p>5.3. Своевременно оплачивать услуги по размещению.\r\n                </p><ol>\r\n                    <li><strong>Порядок расчетов</strong></li>\r\n                </ol><p>6.1. Стоимость услуги размещения варьируется от количества полученных откликов с сайта <a href=\"http://autOK.ru/\">http://autOK.pro/</a> . Само размещение является бесплатным, а каждое\r\n                    уведомление путем SMS оповещения, сообщениями в програмно-информационном комплексе или уведомлениями\r\n                    в мобильном приложении имеет стоимость 40 рублей за каждое уведомление *(данное предложение действительно только в промо-период). В конце отчетного периода\r\n                    формируется акт выполненных работ и счет на оплату. Оплата счета Пользователем происходит в течении\r\n                    2-х банковских дней с момента получения счета.\r\n                </p><p>6.2. Оплата за оказанные услуги перевозки производится:\r\n                </p><p>6.2.1 Путем безналичного перечисления на реквизиты расчетного счета указанные в разделе Контакты на\r\n                    сайте<a href=\"http://autOK.ru/\">http://autOK.pro/</a>\r\n                </p><ol>\r\n                    <li><strong>Ответственность Сторон</strong></li>\r\n                </ol><p>7.1. Стороны несут ответственность за невыполнение своих обязательств в соответствии с действующим\r\n                    законодательством РФ.\r\n                </p><p>7.2. Сайт <a href=\"http://autOK.ru/\">http://autOK.pro/</a> не несет ответственность за перерывы в\r\n                    оказании Услуг в случае сбоев программного обеспечения или оборудования, не принадлежащих сайту <a href=\"http://autOK.ru/\">http://autOK.pro/</a> .\r\n                </p><p>7.3. Сайт <a href=\"http://autOK.ru/\">http://autOK.pro/</a> не несет ответственность за полные или\r\n                    частичные прерывания оказания Услуг, связанные с заменой оборудования, программного обеспечения или\r\n                    проведения других работ, вызванных необходимостью поддержания работоспособности и развития\r\n                    технических средств сайта <a href=\"http://autOK.ru/\">http://autOK.pro/</a> .\r\n                </p><p>7.4. Сайт <a href=\"http://autOK.ru/\">http://autOK.pro/</a> не несет ответственность за прямые убытки,\r\n                    упущенную выгоду, понесенные Пользователем или Клиентом в ходе оказания услуг Пользователем Клиенту.\r\n                </p><p>7.5. Сайт <a href=\"http://autOK.ru/\">http://autOK.pro/</a> не несет ответственность за неисполнение\r\n                    либо ненадлежащее исполнение Пользователем своих обязанностей при оказании услуг Клиенту.\r\n                </p><ol>\r\n                    <li><strong>Порядок разрешения споров</strong></li>\r\n                </ol><p>8.1. Споры и разногласия, связанные с исполнением настоящей Оферты, Стороны будут разрешать путем\r\n                    переговоров.\r\n                </p><p>8.2. В случае невозможности урегулирования споров путем переговоров, они подлежат разрешению в суде,\r\n                    в соответствии с действующим законодательством РФ.\r\n                </p><ol>\r\n                    <li><strong>Особые условия</strong></li>\r\n                </ol><p>9.1. Соглашаясь с условиями Оферты в настоящей редакции, Пользователь выражает свое согласие на\r\n                    получение рекламной информации, распространяемой по сетям связи в целях и случаях, когда\r\n                    необходимость такого согласия предусмотрена законодательством РФ о рекламе.\r\n                </p><ol>\r\n                    <li><strong>Персональные данные</strong></li>\r\n                </ol><p>10.1. Размещаясь на сайте <a href=\"http://autOK.ru/\">http://autOK.pro/</a> , Пользователь дает\r\n                    согласие на сбор и обработку персональных данных о себе, в целях исполнения условий настоящей\r\n                    Оферты.\r\n                </p><p>10.2. Сайт <a href=\"http://autOK.ru/\">http://autOK.pro/</a> собирает и обрабатывает персональные\r\n                    данные Пользователя и Клиента (фамилия, имя и отчество; адреса; контактные телефоны), в целях\r\n                    выполнения условий настоящей Оферты.\r\n                </p><p>10.3. При сборе и обработке персональных данных Пользователей и Клиентов сайт <a href=\"http://autOK.ru/\">http://autOK.pro/</a> не преследует иных целей, кроме установленных в\r\n                    п. 10.2. настоящей Оферты.\r\n                </p><p>10.4. Доступ к персональным данным Пользователей и Клиентов имеют только лица, имеющие\r\n                    непосредственное отношение к исполнению заказа.\r\n                </p><p>* промо-период действует до момента заполнения каталога сайта действующими клиентами в количестве 20 штук. Для этих клиентов условия не меняются вплоть до подписания сторонами дополнительного соглашения на эти изменения. Для остальных клиентов стоимость размещения составляет 700 рублей в месяц.</p>','','','pravila-okazaniya-informacionnyh-uslug','1528318346','1528318346',1),
(5,'Контакты','<p>Вы находитесь на странице КОНТАКТЫ сайта autOK.pro  Если у вас возникли вопросы или предложения по работе нашего сервиса, вы можете написать нам </p><p>на адрес электронной почты: info@autok.pro или позвонить по телефону: +7(950)757-91-79. Спасибо что вы с нами!</p><p><br></p><p>Наши реквизиты: ИП Евсюков И.А. ИНН 366602643775 р/с 40802 810 7024 8000 0904 в АО \"АЛЬФА-БАНК\" БИК 044525593 к/с 30101 810 2000 0000 0593</p>','','','kontakty','1528318429','1528318429',1);

UNLOCK TABLES;

/*Table structure for table `service` */

CREATE TABLE `service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(300) DEFAULT NULL,
  `seo_keyword` varchar(300) DEFAULT NULL,
  `seo_description` varchar(300) DEFAULT NULL,
  `description` text,
  `address` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `g_maps_address` varchar(300) DEFAULT NULL,
  `slug` varchar(300) DEFAULT NULL,
  `car_manufacturer_id` varchar(300) DEFAULT NULL,
  `discount` int(11) DEFAULT NULL,
  `created_at` varchar(300) DEFAULT NULL,
  `updated_at` varchar(300) DEFAULT NULL,
  `is_status` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/*Data for the table `service` */

LOCK TABLES `service` WRITE;

insert  into `service`(`id`,`name`,`seo_keyword`,`seo_description`,`description`,`address`,`email`,`phone`,`g_maps_address`,`slug`,`car_manufacturer_id`,`discount`,`created_at`,`updated_at`,`is_status`) values 
(1,'VAG-сервис','','','<p>Технический центр VAG service специализируется на ремонте и обслуживании автомобилей Audi, Volkswagen, Skoda, а так же Mitsubishi, Kia, Hyundai.</p><p>Современное техническое оборудование и специализированный инструмент в сочетании с большим опытом сотрудников позволяет производить ремонт любой сложности автомобилей данных марок.</p><p>Наш Автотехцентр предоставляет помощь в подборе оригинальных запчастей и качественных запчастей других производителей.</p><p>На все виды работ предоставляется гарантия.</p>','Г ВОРОНЕЖ, УЛ СОЛНЕЧНАЯ, Д 33В','vag-36@mail.ru','+7(950)757-9179','{\"lat\":55.61615065809564,\"lng\":37.46123296642304}','dsfds','[\"2\"]',15,NULL,'1529861170',0),
(2,'fdg','rty','try','<p>fdgdf</p>','fdg','ty@mail.ru','fdg','{\"lat\":55.749317559749244,\"lng\":37.62053472423554}','dfgfd','[\"2\"]',10,'1529534406','1529534406',1),
(3,'fddf','sd','','<p>hjvbhjbh</p>','fg','bjbjhdf@mail.ru','fdg','{\"lat\":55.749317559749244,\"lng\":37.62053472423554}','fddf','[\"6\"]',15,'1529857500','1529857500',1),
(4,'fddf','sd','','<p>hjvbhjbh</p>','fg','bjbjhdf@mail.ru','fdg','{\"lat\":55.749317559749244,\"lng\":37.62053472423554}','fddf-2','[\"6\"]',15,'1529857673','1529857673',1),
(5,'fddf','sd','','<p>hjvbhjbh</p>','fg','bjbjhdf@mail.ru','fdg','{\"lat\":55.749317559749244,\"lng\":37.62053472423554}','fddf-3','[\"6\"]',15,'1529857697','1529857697',1),
(6,'fddf','sd','','<p>hjvbhjbh</p>','fg','bjbjhdf@mail.ru','fdg','{\"lat\":55.749317559749244,\"lng\":37.62053472423554}','fddf-4','[\"6\"]',15,'1529857819','1529857819',1),
(7,'fddf','sd','','<p>hjvbhjbh</p>','fg','bjbjhdf@mail.ru','fdg','{\"lat\":55.749317559749244,\"lng\":37.62053472423554}','fddf-5','[\"6\"]',15,'1529857857','1529857857',1),
(8,'fddf','sd','','<p>hjvbhjbh</p>','fg','bjbjhdf@mail.ru','fdg','{\"lat\":55.749317559749244,\"lng\":37.62053472423554}','fddf-6','[\"6\"]',15,'1529857926','1529857926',1),
(9,'fddf','sd','','<p>hjvbhjbh</p>','fg','bjbjhdf@mail.ru','fdg','{\"lat\":55.749317559749244,\"lng\":37.62053472423554}','fddf-7','[\"1\",\"3\",\"6\"]',15,'1529857949','1529883365',1);

UNLOCK TABLES;

/*Table structure for table `services_image` */

CREATE TABLE `services_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `img_name` varchar(100) DEFAULT NULL,
  `alt` varchar(300) DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `service_id` (`service_id`),
  CONSTRAINT `services_image_ibfk_1` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `services_image` */

LOCK TABLES `services_image` WRITE;

insert  into `services_image`(`id`,`img_name`,`alt`,`service_id`) values 
(1,'fdgdf.jpg',NULL,1),
(3,'Q-xwJvealXgjmo7gtT7_FvgkHrGjDZUd.jpg','fddf',9),
(4,'y_nSqajO_dy3KSmn4_LlpIUgUntUq8dQ.png','fddf',9),
(5,'M9UzN_rzVKqSL3IKrgySFix1XQwgRAb3.png','fddf',9),
(6,'W-IuIgD6n43pBnkl2m4GAWEP0-XFLo9H.png','fddf',9);

UNLOCK TABLES;

/*Table structure for table `user` */

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `user` */

LOCK TABLES `user` WRITE;

insert  into `user`(`id`,`username`,`auth_key`,`password_hash`,`password_reset_token`,`email`,`status`,`created_at`,`updated_at`) values 
(1,'admin','78DcOym98949DhqgTssdvVWnOFWGmEjb','$2y$13$8UkW1Hz24/ywhGY8GoHwLu4vf6tKGLweUNTVGrusTrMP3WgtASXqK',NULL,'admin@mail.ru',10,1528045946,1528045946);

UNLOCK TABLES;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
