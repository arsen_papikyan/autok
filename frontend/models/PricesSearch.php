<?php
/**
 * Created by PhpStorm.
 * User: Arsen
 * Date: 1/07/2018
 * Time: 7:15 PM
 */

namespace frontend\models;

use common\models\Prices;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class PricesSearch extends Prices
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'service_id', 'category_id', 'manufacturer_id'], 'integer'],
            [['description', 'passenger', 'offroad'], 'safe'],
        ];
    }
    
    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }
    
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($servicesId)
    {
        $query = Prices::find()->where(['service_id'=>$servicesId]);
        
        $dataProvider = new ActiveDataProvider(['query' => $query,]);
        
        if (!$this->validate()) {
            return $dataProvider;
        }
    
        $this->load(\Yii::$app->request->queryParams);
    
    
        // grid filtering conditions
        $query->andFilterWhere(
            [
                'id'              => $this->id,
                'service_id'      => $this->service_id,
                'category_id'     => $this->category_id,
                'manufacturer_id' => $this->manufacturer_id,
            ]
        );
        
        $query->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'passenger', $this->passenger])
            ->andFilterWhere(['like', 'offroad', $this->offroad]);
        
        return $dataProvider;
    }
}
