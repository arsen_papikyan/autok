<?php
/**
 * Created by PhpStorm.
 * User: dima
 * Date: 24.03.18
 * Time: 15:59
 */

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

?>
    <div class="request-form">
        <?php Pjax::begin();?>
        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'phone')->textInput(['maxlength' => true, 'id' => 'phone']) ?>

        <?php
        foreach ($car_models as $one){
            $cars[$one->id] = $one->manufacturer->name." ".$one->name;
        }
        ?>

        <?= $form->field($model, 'car_model_id')
            ->widget(Select2::classname(),
                [
                    'data' => $cars,
                    'language' => 'ru',
                    'options' => [
                        'placeholder' => 'Выберите модель автомобиля ...',
                    ],
                    'pluginOptions' => [
                        'tags' => true,
                    ],
                ]
            );
        ?>

        <?= $form->field($model, 'car_service_id')->hiddenInput(['value' => $service_id])->label(false); ?>

        <div class="form-group">
            <?= Html::submitButton('Отправить заявку', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>
        <?php Pjax::end();?>
    </div>
