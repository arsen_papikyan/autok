<?php

namespace frontend\widgets;

use yii\base\Widget;
use common\models\Menu as MenuModel;

class Menu extends Widget
{
    /**
     * @inheritdoc
     */
    public function run()
    {
        $model =
            MenuModel::find()
                ->where(['is_status' => true])
                ->asArray()
                ->all();

        return $this->render(
            'menu',
            [
                'model' => $model
            ]
        );
    }
}