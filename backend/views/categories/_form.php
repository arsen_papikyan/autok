<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\switchinput\SwitchInput;
use yii\helpers\Url;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\Categories */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="categories-form">

    <?php $form = ActiveForm::begin([
        'enableClientValidation' => true,
        'enableAjaxValidation' => false,
        'validateOnChange' => true,
        'validateOnBlur' => false,
        'options' => [
            'enctype' => 'multipart/form-data',
        ]
    ]); ?>

    <div class="col-xs-12">
        <?= $form->field($model, 'is_status')->widget(SwitchInput::classname(), [
            'value' => true,
            'pluginOptions' => [
                'size' => 'large',
                'onColor' => 'success',
                'offColor' => 'danger',
                'onText' => 'Active',
                'offText' => 'Inactive'
            ]
        ]) ?>
    </div>
   <div class="col-xs-12">
        <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'keywords')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'description')->textarea(['rows' => 3]) ?>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <?php
        $model_id = $model->id;
        $uploadImagesUrl = Url::to(['/categories/upload-images?id=' . $model_id]);

        $imagesOptions = [];
        $imgPath = [];

        if (!$model->isNewRecord) {
            $imgName = $model->img_name;
            $imgFullPath = Yii::getAlias("@frontend") . "/web/images/categories/" . $imgName;

            if (!empty($imgName)) {
                $deleteUrl = Url::to(["/categories/delete-file?id=" . $model->id]);

                $imgPath[] = Url::to('/frontend/web/images/categories/') . $imgName;
                $size = 0;
                if (file_exists($imgFullPath)) {
                    $size = filesize($imgFullPath);
                }
                $imagesOptions[] = [
//                'caption' => $model->title,
                    'url' => $deleteUrl,
                    'size' => $size,
                    'key' => $model_id,
                ];
            }
        }
        ?>

        <?= $form->field($model, 'img_name')->widget(FileInput::class,
            [
                'attribute' => 'img_name',
                'name' => 'img_name',
                'options' =>
                    [
                        'accept' => 'image/*',
                        'multiple' => false,
                    ],
                'pluginOptions' => [
                    'previewFileType' => 'image',
                    "uploadAsync" => true,
                    'showPreview' => true,
                    'showUpload' => $model->isNewRecord ? false : true,
                    'showCaption' => false,
                    'showDrag' => false,
                    'uploadUrl' => $uploadImagesUrl,
                    'initialPreviewConfig' => $imagesOptions,
                    'initialPreview' => $imgPath,
                    'initialPreviewAsData' => true,
                    'initialPreviewShowDelete' => true,
                    'overwriteInitial' => true,
                    'resizeImages' => true,
                    'layoutTemplates' =>
                        [
                            !$model->isNewRecord ?: 'actionUpload' => '',
                        ],
                ],
            ]);
        ?>

    </div>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
