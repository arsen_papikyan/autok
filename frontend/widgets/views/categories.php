<?php

use yii\helpers\Html;

?>
<div class="specials-wrap">
    <div class="cont specials">
        <h2>Виды работ</h2>
        <div class="specials-list">
            <div class="row">
                <?php
                $homeUrl = Yii::$app->homeUrl;
                foreach ($model as $category):?>
                    <div class="special col-xs-12 col-sm-4 col-md-4 col-lg-4">

                        <a href="<?= $homeUrl ?>services/<?= $category['slug'] ?>" class="special-link">
                            <p class="special-img">
                                <?= Html::img("/images/categories/{$category['img_name']}") ?>
                            </p>
                            <h3><span><?= $category['title'] ?></span></h3>
                        </a>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        <span class="special-line1"></span>
        <span class="special-line2"></span>
    </div>
</div>