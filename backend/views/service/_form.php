<?php

use common\models\CarModel;
use kartik\file\FileInput;
use kartik\select2\Select2;
use kartik\switchinput\SwitchInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\redactor\widgets\Redactor;
use yii\widgets\ActiveForm;
use common\models\Categories;

/* @var $this yii\web\View */
/* @var $model common\models\Service */
/* @var $productImages common\models\ServicesImage */
/* @var $form yii\widgets\ActiveForm */

$model_id = $model->id
?>

<div class="service-form">
    <?php
    $form = ActiveForm::begin(
        [
            'enableClientValidation' => true,
            'enableAjaxValidation'   => false,
            'validateOnChange'       => true,
            'validateOnBlur'         => false,
            'options'                => [
                'enctype' => 'multipart/form-data',
                'id'      => 'dynamic-form',
            ],
        ]
    );
    ?>

    <div class="col-xs-12">
        <?=
        $form->field($model, 'is_status')->widget(
            SwitchInput::class,
            [
                'value'         => true,
                'pluginOptions' =>
                    [
                        'size'     => 'large',
                        'onColor'  => 'success',
                        'offColor' => 'danger',
                        'onText'   => 'Active',
                        'offText'  => 'Inactive',
                    ],
            ]
        )
        ?>
    </div>
    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'seo_keyword')->textInput(
                    ['maxlength' => true]
                ) ?>
            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'seo_description')->textarea(
                    ['row' => 3]
                ) ?>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'name')->textInput(
                    ['maxlength' => true]
                ) ?>
            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'email')->textInput(
                    ['maxlength' => true]
                ) ?>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'address')->textInput(
                    ['maxlength' => true]
                ) ?>
            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'phone')->textInput(
                    ['maxlength' => true]
                ) ?>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?php
                if (!$model->isNewRecord){
                    $model->categoryId =
                        ArrayHelper::map(
                                \common\models\CarService::find()
                                ->where(['service_id'=>$model_id])
                                ->asArray()
                                ->all(),
                                'id',
                                'category_id'
                        );
                }
                $categoryModel =
                    ArrayHelper::map(
                        Categories::find()->all(),
                        'id',
                        'title'
                    );
                
                echo $form->field($model, 'categoryId')->widget(
                    Select2::class,
                    [
                        'data'    => $categoryModel,
                        'options' => ['multiple' => true, 'placeholder' => 'Categories'],
                    ]
                );
                ?>
            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'discount')->textInput() ?>
            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <?php
        $carModel =
            ArrayHelper::map(
                CarModel::find()->all(),
                'id',
                'title'
            );
        echo $form->field($model, 'car_manufacturer_id')->widget(
            Select2::class,
            [
                'data'    => $carModel,
                'options' => ['multiple' => true, 'placeholder' => 'Car Manufacturer'],
            ]
        );
        ?>
    </div>

    <div class="col-xs-12">
        <?= $form->field($model, 'description')->widget(
            Redactor::class,
            [
                'clientOptions' =>
                    [
                        'imageUpload' => Url::to(['/redactor/upload/image']),
                        'fileUpload'  => false,
                        'plugins'     =>
                            [
                                'fontcolor',
                                'imagemanager',
                                'table',
                                'undoredo',
                                'clips',
                                'fullscreen',
                            ],
                    ],
            ]
        );
        ?>
    </div>
    <div class="col-xs-12">

    </div>
    <div class="col-xs-12">
        <?php
        $uploadImages = Url::to(['/service/upload-images?id=' . $model_id]);
        $imagesOptions = [];
        $imgPath = [];
        $size = 0;
        
        if (!$model->isNewRecord) {
            $imgFullPath = Yii::getAlias("@frontend") . "/web/images/services/";
            
            foreach ($dataProductImages as $val) {
                $imgName = $val['img_name'];
                $deleteUrl = Url::to(["/service/delete-file?id=" . $val['id']]);
                
                if (file_exists($imgFullPath . $imgName)) {
                    $size = filesize($imgFullPath . $imgName);
                    $imgPath[] =
                        Url::to('/frontend/web/images/services/' . $imgName);
                }
                $imagesOptions[] = [
                    //  'caption' => $model->title,
                    'url'  => $deleteUrl,
                    'size' => $size,
                    'key'  => $model_id,
                
                ];
            }
        }
        
        ?>
        <?= $form->field($productImages, 'img_name[]')->widget(
            FileInput::class,
            [
                'attribute'     => 'img_name[]',
                'name'          => 'img_name[]',
                'options'       =>
                    [
                        'accept'   => 'image/*',
                        'multiple' => true,
                    ],
                'pluginOptions' =>
                    [
                        'previewFileType'          => 'image',
                        "uploadAsync"              => true,
                        'showPreview'              => true,
                        'showUpload'               => $model->isNewRecord ? false : true,
                        'showCaption'              => false,
                        'showDrag'                 => false,
                        'uploadUrl'                => $uploadImages,
                        'initialPreviewConfig'     => $imagesOptions,
                        'initialPreview'           => $imgPath,
                        'initialPreviewAsData'     => true,
                        'initialPreviewShowDelete' => true,
                        'overwriteInitial'         => $model->isNewRecord ? true : false,
                        'resizeImages'             => true,
                        'layoutTemplates'          => [!$model->isNewRecord ?: 'actionUpload' => '',],
                    ],
            ]
        );
        ?>
    </div>
    
    <?= $form->field($model, 'g_maps_address')->hiddenInput()->label('Google Map') ?>

    <div id="map"></div>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
