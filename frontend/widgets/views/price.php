<?php

use common\models\CarManufacturer;
use common\models\Categories;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

\yii\widgets\Pjax::begin();

echo GridView::widget(
    [
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            [
                'attribute'   => 'manufacturer_id',
                'label'       => 'Производитель <i class="fas fa-sort"></i>',
                'encodeLabel' => false,
                'value'       => 'manufacturer.name',
                'filter'      =>
                    ArrayHelper::map(
                        CarManufacturer::find()
                            ->where(['id' => array_column($dataProvider->models, 'manufacturer_id')])
                            ->asArray()
                            ->all(),
                        'id',
                        'name'
                    ),
            ],
            [
                'attribute'   => 'category_id',
                'label'       => 'Услуга <i class="fas fa-sort"></i>',
                'encodeLabel' => false,
                'value'       => 'category.title',
                'filter'      =>
                    ArrayHelper::map(
                        Categories::find()
                            ->where(['id' => array_column($dataProvider->models, 'category_id')])
                            ->asArray()->all(),
                        'id',
                        'title'
                    ),
            ],
            [
                'attribute'      => 'passenger',
                'label'          => 'Легковой, <i class="fas fa-ruble-sign"></i> <i class="fas fa-sort"></i> *',
                'encodeLabel'    => false,
                'filter'         => false,
                'contentOptions' => ['class' => 'value'],
                'value'          => function ($model) {
                    if (!empty($model->passenger)) {
                        return $model->passenger . ' (без учета скидки)';
                    }
                    
                    return null;
                },
            ],
            [
                'attribute'      => 'offroad',
                'label'          => 'Внедорожник, <i class="fas fa-ruble-sign"></i> <i class="fas fa-sort"></i> *',
                'encodeLabel'    => false,
                'filter'         => false,
                'contentOptions' => ['class' => 'value'],
                'value'          => function ($model) {
                    if (!empty($model->offroad)) {
                        return $model->offroad . ' (без учета скидки)';
                    }
                    
                    return null;
                },
            ],
            [
                'attribute'      => 'description',
                'label'          => 'Описание',
                'encodeLabel'    => false,
                'filter'         => false,
                'contentOptions' => ['class' => 'value'],
            ],
        ],
    ]
);

\yii\widgets\Pjax::end();


