<?php

namespace backend\controllers;

use backend\models\ServiceControl;
use common\models\Service;
use common\models\ServicesImage;
use Imagine\Image\Box;
use Yii;
use yii\filters\VerbFilter;
use yii\imagine\Image;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * ServiceController implements the CRUD actions for Service model.
 */
class ServiceController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    /**
     * Lists all Service models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ServiceControl();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        return $this->render(
            'index',
            [
                'searchModel'  => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        );
    }
    
    /**
     * Displays a single Service model.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render(
            'view',
            [
                'model' => $this->findModel($id),
            ]
        );
    }
    
    /**
     * Finds the Service model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return Service the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Service::findOne($id)) !== null) {
            return $model;
        }
        
        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    /**
     * Creates a new Service model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Service();
        $productImages = new ServicesImage();
        $model->g_maps_address = '{"lat":55.749317559749244,"lng":37.62053472423554}';
        $data = [];
        
        if ($model->load(Yii::$app->request->post())) {
            $model->car_manufacturer_id = json_encode($model->car_manufacturer_id);
            
            if ($model->save()) {
                foreach ($model->categoryId as $key => $category) {
                    $model->categoryId[$key] =
                        [
                            $category,
                            $model->id,
                        ];
                }
                $imgFile = UploadedFile::getInstances($productImages, "img_name");
                
                if (!empty($imgFile)) {
                    $imgPath = Yii::getAlias("@frontend") . "/web/images/services/";
                    foreach ($imgFile as $file) {
                        $imgName = sprintf('%s.%s', Yii::$app->security->generateRandomString(), $file->extension);
                        $file->saveAs($imgPath . $imgName);
                        
                        /**
                         * compress image, and  change size image
                         */
                        $path = sprintf('%s%s', $imgPath, $imgName);
                        $image = Image::getImagine()->open($path);
                        
                        $width = $image->getSize()->getWidth() >= 800 ? 800 : $image->getSize()->getWidth();
                        $height = $image->getSize()->getHeight() >= 600 ? 600 : $image->getSize()->getWidth();
                        
                        $image->thumbnail(new Box($width, $height))
                            ->save($path, ['jpeg_quality' => 50])
                            ->save($path, ['png_compression_level' => 7]);
                        
                        $image = Image::getImagine()->open($path);
                        $path = sprintf('%ssmall/%s', $imgPath, $imgName);
                        $image->thumbnail(new Box($width, $height))
                            ->save($path, ['jpeg_quality' => 40])
                            ->save($path, ['png_compression_level' => 7]);
                        
                        $data[] =
                            [
                                $imgName,
                                $model->name,
                                $model->id,
                            ];
                    }
                    
                    Yii::$app->db->createCommand()
                        ->batchInsert(
                            "services_image",
                            ['img_name', 'alt', 'service_id'],
                            $data
                        )->execute();
                }
                
                Yii::$app->db->createCommand()
                    ->batchInsert(
                        "car_service",
                        ['category_id', 'service_id'],
                        $model->categoryId
                    )->execute();
                
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        
        return
            $this->render(
                'create',
                [
                    'model'         => $model,
                    'productImages' => $productImages,
                ]
            );
    }
    
    /**
     * Updates an existing Service model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $productImages = new ServicesImage();
        $dataProductImages = ServicesImage::findAll(['service_id' => $id]);
        $model->car_manufacturer_id = json_decode($model->car_manufacturer_id);
        $data = [];
        
        if ($model->load(Yii::$app->request->post())) {
            $model->car_manufacturer_id = json_encode($model->car_manufacturer_id);
    
            if ($model->save()) {
                $imgFile = UploadedFile::getInstances($productImages, "img_name");
                
                if (!empty($imgFile)) {
                    $imgPath = Yii::getAlias("@frontend") . "/web/images/services/";
                    foreach ($imgFile as $file) {
                        $imgName = Yii::$app->security->generateRandomString() . '.' . $file->extension;
                        $file->saveAs($imgPath . $imgName);
                        
                        /**
                         * compress image, and  change size image
                         */
                        $path = $imgPath . $imgName;
                        $image = Image::getImagine()->open($path);
                        
                        $width = $image->getSize()->getWidth() >= 800 ? 800 : $image->getSize()->getWidth();
                        $height = $image->getSize()->getHeight() >= 600 ? 600 : $image->getSize()->getWidth();
                        
                        $image->thumbnail(new Box($width, $height))
                            ->save($path, ['jpeg_quality' => 50])
                            ->save($path, ['png_compression_level' => 7]);
                        
                        $image = Image::getImagine()->open($path);
                        
                        /* small (changes img size) */
                        $width = $width >= 200 ? 200 : $width;
                        $height = $height >= 150 ? 150 : $height;
                        
                        $path = $imgPath . "small/" . $imgName;
                        $image->thumbnail(new Box($width, $height))
                            ->save($path, ['jpeg_quality' => 40])
                            ->save($path, ['png_compression_level' => 7]);
                        $data[] =
                            [$imgName, $model->name, $model->id];
                        
                    }
                    Yii::$app->db->createCommand()
                        ->batchInsert(
                            "services_image",
                            ['img_name', 'alt', 'service_id'],
                            $data
                        )
                        ->execute();
                }
                
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        
        return $this->render(
            'update',
            [
                'model'             => $model,
                'productImages'     => $productImages,
                'dataProductImages' => $dataProductImages,
            ]
        );
    }
    
    /**
     * Deletes an existing Service model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        
        return $this->redirect(['index']);
    }
    
    public function actionUploadImages($id)
    {
        $products = $this->findModel($id);
        $model = new ServicesImage();
        $data = [];
        
        $imgFile = UploadedFile::getInstances($model, "img_name");
        
        if (!empty($imgFile)) {
            $imgPath = Yii::getAlias("@frontend") . "/web/images/services/";
            foreach ($imgFile as $file) {
                $imgName = Yii::$app->security->generateRandomString() . '.' . $file->extension;
                $file->saveAs($imgPath . $imgName);
                
                /**
                 * compress image, and  change size image
                 */
                $path = $imgPath . $imgName;
                $image = Image::getImagine()->open($path);
                
                $width = $image->getSize()->getWidth() >= 800 ? 800 : $image->getSize()->getWidth();
                $height = $image->getSize()->getHeight() >= 600 ? 600 : $image->getSize()->getWidth();
                
                $image->thumbnail(new Box($width, $height))
                    ->save($path, ['jpeg_quality' => 50])
                    ->save($path, ['png_compression_level' => 7]);
                
                /* small (changes img size) */
                $width = $width >= 200 ? 200 : $width;
                $height = $height >= 150 ? 150 : $height;
                
                $path = $imgPath . "small/" . $imgName;
                $image->thumbnail(new Box($width, $height))
                    ->save($path, ['jpeg_quality' => 40])
                    ->save($path, ['png_compression_level' => 7]);
                
                $data[] =
                    [$imgName, $products->name, $products->id];
                
            }
            
            Yii::$app->db->createCommand()
                ->batchInsert(
                    "services_image",
                    ['img_name', 'alt', 'service_id'],
                    $data
                )
                ->execute();
        }
        
        return true;
    }
}
