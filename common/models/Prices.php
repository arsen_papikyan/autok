<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "prices".
 *
 * @property int $id
 * @property int $service_id
 * @property int $category_id
 * @property int $manufacturer_id
 * @property string $description
 * @property string $passenger
 * @property string $offroad
 *
 * @property Service $service
 * @property Categories $category
 * @property CarManufacturer $manufacturer
 */
class Prices extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'prices';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['service_id', 'category_id', 'manufacturer_id'], 'integer'],
            [['description', 'passenger', 'offroad'], 'string', 'max' => 300],
            [['service_id'], 'exist', 'skipOnError' => true, 'targetClass' => Service::className(), 'targetAttribute' => ['service_id' => 'id']],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Categories::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['manufacturer_id'], 'exist', 'skipOnError' => true, 'targetClass' => CarManufacturer::className(), 'targetAttribute' => ['manufacturer_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'service_id' => 'Service ID',
            'category_id' => 'Category ID',
            'manufacturer_id' => 'Manufacturer ID',
            'description' => 'Description',
            'passenger' => 'Passenger',
            'offroad' => 'Offroad',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getService()
    {
        return $this->hasOne(Service::className(), ['id' => 'service_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Categories::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManufacturer()
    {
        return $this->hasOne(CarManufacturer::className(), ['id' => 'manufacturer_id']);
    }
}
