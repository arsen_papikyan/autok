<?php
/**
 * Created by PhpStorm.
 * User: dima
 * Date: 24.03.18
 * Time: 15:57
 */

namespace frontend\widgets;


use app\models\CarModel;
use app\models\CarService;
use app\models\Request;
use kartik\alert\Alert;
use Yii;
use yii\base\Widget;
use yii\helpers\ArrayHelper;

class RequestPopUp extends Widget
{

    public $service_id;
    public function run()
    {
        $model = new Request();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->view->registerJs('$(".panel-collapse").addClass( "in" );');
            echo Alert::widget([
                'type' => Alert::TYPE_SUCCESS,
                'title' => 'Заявка отправлена!',
                'icon' => 'glyphicon glyphicon-ok-sign',
                'body' => 'Менеджер автосервиса свяжется с вами в ближайшее время.',
                'showSeparator' => true,
                'delay' => 20000
            ]);
        }

        $car_models_array = CarModel::find()
            ->joinWith(
                'manufacturer',
                true,
                'LEFT JOIN'
            )
            ->all();

        return $this->render('request', [
            'service_id' => $this->service_id,
            'model' => $model,
            'car_models' => $car_models_array
        ]);
    }

}