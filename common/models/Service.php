<?php

namespace common\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "service".
 *
 * @property int $id
 * @property string $name
 * @property string $seo_keyword
 * @property string $seo_description
 * @property string $description
 * @property string $address
 * @property string $email
 * @property string $phone
 * @property string $g_maps_address
 * @property string $slug
 * @property string $car_manufacturer_id
 * @property int $discount
 * @property int $is_status
 *
 * @property CarService[] $carServices
 * @property ServicesImage[] $servicesImages
 */
class Service extends ActiveRecord
{
    public $categoryId;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'service';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['email', 'phone','categoryId','car_manufacturer_id'], 'required'],
            [['discount', 'is_status',], 'integer'],
            [['name', 'seo_keyword', 'seo_description', 'g_maps_address', 'slug'], 'string', 'max' => 300],
            [['address', 'email'], 'string', 'max' => 255],
            [['phone'], 'string', 'max' => 15],
            [['car_manufacturer_id'], 'safe', ],
        ];
    }

    public function behaviors()
    {
        return [
            'slug' => [
                'class' => 'Zelenin\yii\behaviors\Slug',
                'slugAttribute' => 'slug',
                'attribute' => 'name',
                // optional params
                'ensureUnique' => true,
                'replacement' => '-',
                'lowercase' => true,
                'immutable' => true,
                // If intl extension is enabled, see http://userguide.icu-project.org/transforms/general.
                'transliterateOptions' => 'Russian-Latin/BGN; Any-Latin; Latin-ASCII; NFD; [:Nonspacing Mark:] Remove; NFC;'
            ],
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'seo_keyword' => 'Seo Keyword',
            'seo_description' => 'Seo Description',
            'description' => 'Description',
            'address' => 'Address',
            'email' => 'Email',
            'phone' => 'Phone',
            'g_maps_address' => 'G Maps Address',
            'slug' => 'Slug',
            'car_manufacturer_id' => 'Car Manufacturer',
            'discount' => 'Discount',
            'is_status' => 'Is Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarServices()
    {
        return $this->hasMany(CarService::class, ['service_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServicesImages()
    {
        return $this->hasMany(ServicesImage::class, ['service_id' => 'id']);
    }
}
