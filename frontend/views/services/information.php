<?php

/* @var $this yii\web\View */

/* @var $model \common\models\Service */

use common\models\CarManufacturer;
use common\models\CarService;
use metalguardian\fotorama\Fotorama;
use yii\bootstrap\Collapse;
use yii\helpers\Html;

$this->title = 'Автосервис ' . $model->name;
?>
<main>
    <div class="maincont">
        <span id="lat_lng"><?= $model->g_maps_address ?></span>
        <div class="post-map" id="post-map1"></div>
        <div class="cont">
            <div class="post">
                <div class="post-info">
                    <span><?= $model->address; ?></span>
                    <h1><?= $model->name; ?></h1>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <?php
                        if (!empty($model->servicesImages)) {
                            $fotorama = Fotorama::begin(
                                [
                                    'options'     =>
                                        [
                                            'loop'  => true,
                                            'hash'  => true,
                                            'ratio' => 1920 / 1080,
                                            'nav'   => 'thumbs',
                                        ],
                                    'spinner'     => [
                                        'lines' => 20,
                                    ],
                                    'tagName'     => 'span',
                                    'useHtmlData' => false,
                                    'htmlOptions' => [
                                        'class' => 'custom-class',
                                        'id'    => 'custom-id',
                                    ],
                                ]
                            );
                            
                            foreach ($model->servicesImages as $img) {
                                echo Html::img(
                                    "/images/services/{$img['img_name']}",
                                    [
                                        'alt' => $img['alt'],
                                    ]
                                );
                            }
                            
                            Fotorama::end();
                        }
                        else {
                            echo Html::tag(
                                'p',
                                'Нет изображений',
                                [
                                    'style' => [
                                        'font-size'  => '40px',
                                        'text-align' => 'center',
                                    ],
                                ]
                            );
                        }
                        ?>
                        <!--                        --><?php //if ($item->is_contract === 1): ?>
                        <!--                        <div id="request">-->
                        <!--                            --><?php //elseif ($item->is_contract === 0): ?>
                        <div id="request" class="fake_request">
                            <!--                                --><?php //endif; ?>
                            <?php
                            try {
                                echo Collapse::widget(
                                    [
                                        'items' => [
                                            [
                                                'options' => ['id' => 'request_form'],
                                                'label'   => 'Отправить заявку на ремонт',
                                                //                                                'content' => RequestPopUp::widget(['service_id' => $model->id]),
                                                'content' => '',
                                            ],
                                        ],
                                    ]
                                );
                            } catch (Exception $e) {
                                echo $e;
                            }
                            ?>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <p>
                            <b>Поддерживаемые марки:</b>
                            <?php
                            $manufacturers = '';
                            
                            $carManufacturer =
                                CarManufacturer::find()
                                    ->where(['id' => json_decode($model->car_manufacturer_id)])
                                    ->asArray()
                                    ->all();
                            
                            foreach ($carManufacturer as $manufacturer) {
                                $manufacturers =
                                    sprintf(
                                        '%s %s',
                                        $manufacturers,
                                        $manufacturer['name']
                                    );
                            }
                            echo $manufacturers;
                            ?>
                        </p>
                        <p>
                            <b>Услуги:</b>
                            <?php
                            $carService =
                                CarService::find()
                                    ->where(['service_id' => $model->id])
                                    ->joinWith('category')
                                    ->asArray()
                                    ->all();
                            $category = '';
                            
                            foreach ($carService as $value) {
                                $category =
                                    sprintf(
                                        '%s %s',
                                        $category,
                                        $value['category']['title']
                                    );
                            }
                            echo $category;
                            ?>
                        </p>
                        <?= $model->description; ?>
                    </div>
                </div>
                <div class="row">
                    <p id="footnote">* без учета скидки</p>
                    <?=  \frontend\widgets\Price::widget(['serviceId'=>$model->id])?>
                </div>
            </div>
        </div>
    </div>
</main>

