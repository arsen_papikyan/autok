<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\switchinput\SwitchInput;
use kartik\select2\Select2;
use common\models\Menu;
use yii\helpers\ArrayHelper;
use common\models\Categories;
use common\models\Pages;

/* @var $this yii\web\View */
/* @var $model common\models\Menu */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="menu-form">
    <?php $form = ActiveForm::begin(); ?>

    <div class="col-xs-12">
        <?= $form->field($model, 'is_status')->widget(SwitchInput::class, [
            'value' => true,
            'pluginOptions' => [
                'size' => 'large',
                'onColor' => 'success',
                'offColor' => 'danger',
                'onText' => 'Active',
                'offText' => 'Inactive'
            ]
        ]);
        ?>
    </div>
    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
            </div>

            <div class="col-xs-6">
                <?php
                if ($model->isNewRecord) {
                    $data = ArrayHelper::map(Menu::find()
                        ->select('id,title')
                        ->where(['is_status' => true])
                        ->asArray()
                        ->all(), "id", "title");
                } else {
                    $data = ArrayHelper::map(Menu::find()
                        ->select('id,title')
                        ->where(['is_status' => true])
                        ->andWhere(['!=', 'id', $model->id])
                        ->asArray()
                        ->all(), "id", "title");
                }

                echo $form->field($model, 'parent_id')->widget(Select2::class, [
                    'data' => $data,
                    'options' => [
                        'placeholder' => 'Parent',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]); ?>


            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'keywords')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'description')->textarea(['rows' => 2]) ?>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <?php

        $pages = ArrayHelper::map(
                Pages::find()->select(['slug', 'title'])
            ->where(['is_status' => 1])
            ->asArray()
            ->all(), 'slug', 'title');
        $temp = [];

        foreach ($pages as $key => $val) {
            $temp["pages/$key"] = $val;
        }


        echo $form->field($model, 'slug')->widget(Select2::class, [
            'data' => $temp,
            'options' => [
                'placeholder' => 'slug',
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
        ?>
    </div>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
