<?php

use common\models\CarManufacturer;
use common\models\CarService;
use yii\helpers\Html;

$homeUrl = Yii::$app->homeUrl;
?>
<div class="sectls">
    <a href='<?= $homeUrl ?>service/information/<?= $model->slug?>' class="sectls-img">
        <?php
        if (!empty($model->servicesImages[0]['img_name'])) {
            echo Html::img('/images/services/small/' . $model->servicesImages[0]['img_name']);
        }
        ?>
    </a>
    <div class="sectls-cont">
        <?php if ($model->discount != null): ?>
            <div class="discount-lable"><p>Скидка <?= $model->discount ?>%</p></div>
        <?php endif; ?>
        <div class="sectls-ttl-wrap">
            <p><?= $model->address; ?></p>
            <h3><a href='<?= $homeUrl ?>service/information/<?= $model->slug ?>'><?= $model->name; ?></a></h3>
        </div>
        <div class="sectls-price-wrap">
            <p>Услуги</p>
            <p class="sectls-price">
                <?php
                $carService =
                    CarService::find()
                        ->where(['service_id' => $model->id])
                        ->joinWith('category')
                        ->asArray()
                        ->all();
                $category = '';
                
                foreach ($carService as $value) {
                    $category =
                        sprintf(
                            '%s %s',
                            $category,
                            $value['category']['title']
                        );
                }
                echo $category;
                ?>
        </div>
    </div>
    <div class="sectls-info">
        <p class="sectls-add">
         
            <a href='<?= $homeUrl ?>service/information/<?= $model->slug ?>' class="btn btn-find">Перейти</a>
        </p>
        <p><b>Марки авто:</b>
            <?php
            $manufacturer = '';
            
            $carManufacturer =
                CarManufacturer::find()
                    ->where(['id' => json_decode($model->car_manufacturer_id)])
                    ->asArray()
                    ->all();
            
            foreach ($carManufacturer as $value) {
                $manufacturer =
                    sprintf(
                        '%s %s',
                        $manufacturer,
                        $value['name']
                    );
            }
            echo $manufacturer;
            ?>
        </p>
    </div>
</div>
