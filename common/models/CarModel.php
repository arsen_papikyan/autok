<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "car_model".
 *
 * @property int $id
 * @property string $title
 * @property int $car_manufacturer_id
 *
 * @property CarManufacturer $carManufacturer
 * @property CarService[] $carServices
 */
class CarModel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'car_model';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['car_manufacturer_id'], 'integer'],
            [['title'], 'string', 'max' => 300],
            [['car_manufacturer_id'], 'exist', 'skipOnError' => true, 'targetClass' => CarManufacturer::className(), 'targetAttribute' => ['car_manufacturer_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'car_manufacturer_id' => 'Car Manufacturer ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarManufacturer()
    {
        return $this->hasOne(CarManufacturer::className(), ['id' => 'car_manufacturer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarServices()
    {
        return $this->hasMany(CarService::className(), ['car_model_id' => 'id']);
    }
}
